# ModalNeRF
## [Project page](https://apchenstu.github.io/TensoRF/) |  [Paper](https://arxiv.org/abs/2203.09517)
This repository contains a pytorch implementation for the paper: ModalNeRF : Neural Modal Analysis and Synthesis for Free-Viewpoint Navigation in Dynamically Vibrating Scenes. Our work presents the first approach that allows physically-based editing of motion in a scene captured with a single hand-held video camera, containing vibrating or periodic motion. <br><br>


## Installation

#### Tested on Windows 10 + Pytorch 1.10.1 

Install environment:
```
conda create -n TensoRF python=3.8
conda activate TensoRF
pip install torch torchvision
pip install tqdm scikit-image opencv-python configargparse lpips imageio-ffmpeg kornia lpips tensorboard
```


## Dataset
Datasets are available at: https://repo-sam.inria.fr/fungraph/modalnerf/datasets.zip



## Quick Start
The training script is in `train.py`, to train a ModalNeRF reconstruction:

```
python train.py --config configs/example.txt
```


we provide a few examples in the configuration folder, please note:

 `n_lamb_sigma` and `n_lamb_sh` are string type refer to the basis number of density and appearance along XYZ
dimension;

 `N_voxel_init` and `N_voxel_final` control the resolution of matrix and vector;

 `N_vis` and `vis_every` control the visualization during training;

For more options refer to the `opt.py`. 


## Analysis and synthesis

```
python train.py --config configs/lego.txt --ckpt path/to/your/checkpoint --render_only 1 --render_test 1 
```

You can just simply pass `--render_only 1` and `--ckpt path/to/your/checkpoint` to render images from a pre-trained checkpoint. The results are located in the checkpoint's folder. 

You need to set `--render_test 1`/`--render_path 1` if you want to render testing views or path after training. 

  To perform a modal analysis you need to set `--render_modal n`, n is the number of the camera viewpoint you want to see the analysis from

  Once a modal analysis has been done, set `--render_modal n` and `--load_modes 1` to perform a synthesis

  You can write scripts for the synthesis, set `render_modal n` and `--load_modes 2` to load a script for the synthesis. Synthesis script examples are provided in the forces folder. 

## Training with your own data
We provide two options for training on your own image set:

1. Following the instructions in the [NSVF repo](https://github.com/facebookresearch/NSVF#prepare-your-own-dataset), then set the dataset_name to 'tankstemple'.
2. Calibrating images with the script from [NGP](https://github.com/NVlabs/instant-ngp/blob/master/docs/nerf_dataset_tips.md):
`python dataLoader/colmap2nerf.py --colmap_matcher exhaustive --run_colmap`, then adjust the datadir in `configs/your_own_data.txt`. Please check the `scene_bbox` and `near_far` if you get abnormal results.
    
## Writing synthesis scripts

  The first line of a synthesis script is the name of the synthesis followed by a number. This number is the number of physical simulation steps happening between two frames.

  The next lines are all instructions of the form `5 camera 10`. The first parameter is the frame at which the instruction should be executed. The second parameter is the name of the instruction. The next parameters depend on the instruction.

  `camera 10` puts the camera in the position of the 10th view point in the original video.

  `rigidity 0.7 0.5 0.8` allows you to change the rigidity of each mode. You must give as many parameters as there are modes.

  `pull 400 0.08 -0.12 -0.1 70` allows you to pull the particle 400 in the direction (0.08,-0.12,-0.1) with the intensity 70. Increasing the length of the direction or increasing the intensity leads to the same result but the intensity can be more convenient to use.

  `force 1000 1400 1800` applies a force on each mode. You need as many parameters as there are modes.

  Each instruction can be repeated accross multiple frames by adding `{` before the instruction name and a very last paremeter that indicates the number of frames the instruction should last.

  For example `10 {pull 400 0.08 -0.12 -0.1 70 20` will pull the particle 400 for 20 frames with the intensity increasing from 0 at frame 10 to 70 at frame 29.

  Always end the script with a `end` instruction.


## Citation
If you find our code or paper helps, please consider citing:
```
Add our article bibtex code here

```
