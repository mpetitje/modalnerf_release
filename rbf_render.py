import numpy as np
import math
import shutil
import os
import pathlib
import re
import sys
from tqdm import trange
import time
import matplotlib.pyplot as plt
#from scipy.interpolate import RBFInterpolator

sys.stdout.flush()
from PIL import Image, ImageDraw
import imageio
import torch
import torch.nn as nn
import torch.nn.functional as F
import polyscope as ps
ps.set_use_prefs_file(False)
#from pykeops.torch import LazyTensor

#import free_viewpoint_rendering as fvr
#import train
#import simulation
#import train_modes
#import run_nerf_helpers as helper
#from scipy.interpolate import RBFInterpolator

device = torch.device("cuda" if torch.cuda.is_available() else "cpu")

# Note : My_New_Bender only has one grid. For multi grids, use My_Multi_Opti_Bender
# Other Note : this file is a mess

class My_Multi_Opti_Bender(torch.nn.Module):

    def __init__(self, embed_fn, canonical_sampling, img_sampling, grid_size=40, k=8, radius=-1, margin=1.0, poly_size=None, poly_pos=None):
        # img_sampling : nImg x sample_size x 3

        # permut = torch.randperm(img_sampling.shape[0])
        # st = time.clock()
        # permut = torch.tensor(range(img_sampling.shape[0]))
        super().__init__()
        self.decal = 0
        self.param = img_sampling
        # self.img_sampling = self.param.to(device)
        self.canonical_sampling = canonical_sampling.to(device)
        # print(time.clock() - st, "seconds for the permutation thing.")
        self.k = k
        self.embed_fn = embed_fn
        self.padding = 0

        self.canonical_side = 0

        self.margin = margin
        self.grid_size = grid_size

        self.accuracy_compute = False
        self.accuracy_score = None

        #self.new_c = torch.tensor([-0.3,0.1,0.2],device=device)
        #self.size_crop = 0.35
        if poly_pos is None:
            poly_pos = [0,0,0]
        if poly_size is None:
            poly_size = [1,1,1]
        self.new_c = torch.tensor(poly_pos, device=device)
        self.size_crop = torch.tensor(poly_size, device=device)
        #self.size_crop = poly_size[0]

        # The radius is supposed to be useless
        if radius == -1:
            self.radius = self.margin * 2 / (self.grid_size * 2)
            # If no radius has been given, the radius is built so that it is smaller than a cell
        else:
            self.radius = radius

        self.update_minMax()
        # print("Min, max", self.min, self.max, "epsilon:", ((self.max - self.min) / grid_size) ** 2)
        self.grid = {}
        self.eps = 0.000000001
        # st = time.clock()
        # self.awesome_fill_grid()
        # print(time.clock() - st,"seconds for the awesome fill grid.")
        # self.shape_param = (radius * self.grid_size / (self.max - self.min)) ** 2           # dim : nImg
        self.rigidity_test_time_cutoff = None
        self.test_time_scaling = None
        self.polytope = None


        #self.unfold = torch.nn.Unfold(kernel_size=)

        # self.img_sampling = torch.cat((self.param, torch.tensor([[-100, -100, -100]], device=device).repeat((self.param.shape[0],1,1))), dim=1)
        # self.canonical_sampling = torch.cat((self.canonical_sampling, torch.tensor([[0, 0, 0]], device=device)), dim=0)
        # self.lazy_fill_grid()
        # print(torch.sum(torch.sum(torch.sum(torch.where(self.grid == self.img_sampling.shape[0]-1,0,1),dim=0),dim=0),dim=0) / (grid_size**3))
        # self.pad_grid()

    def substract(self,polidx):
        self.canonical_sampling = self.canonical_sampling[polidx]
        self.param = self.param[:,polidx]

    def generate_sampling(self):
        print("Let's be clear, this function should only be used at the beginning of training, if you see this sentence anywhere else, something has gone wrong.")
        #self.canonical_sampling = torch.rand_like(self.canonical_sampling) * (max[None, :] - min[None, :]) + min[None,:]
        self.canonical_sampling = torch.rand_like(self.canonical_sampling) * 2 - 1
        side = math.floor(math.pow(self.canonical_sampling.shape[0],1/3))
        grid = np.mgrid[0:side,0:side,0:side].transpose((1,2,3,0)).reshape((-1,3))
        self.canonical_sampling = torch.tensor(grid,device=device).view(-1,3) * 2 / side - 1
        self.param = self.param[:,:self.canonical_sampling.shape[0]].detach().clone()
        self.param.requires_grad = True
        self.canonical_side = side

        #self.canonical_sampling[:,0] = torch.
        # self.img_sampling = self.canonical_sampling[None,:,:]  + self.param
        self.update_minMax()
        if False:
            ps.init()
            ps_cloud = ps.register_point_cloud("Input points", self.canonical_sampling[:-1].cpu().detach().numpy())
            for i in range(5):
                ps_cloud.add_scalar_quantity("displacement_mean_" + str(i),
                                             torch.mean(self.img_sampling[i, :-1], dim=-1).cpu().detach().numpy())
            ps_cloud.add_scalar_quantity("displacement_mean_general",
                                         torch.mean(torch.mean(self.img_sampling[:, :-1], dim=-1),
                                                    dim=0).cpu().detach().numpy())
            ps.show()

    # def parameters(self):
    #    return torch.nn.Parameter(self.img_sampling)

    def spatial_reg(self):
        side = self.canonical_side
        d = self.param[self.decal].reshape(side,side,side,3)
        dx = torch.mean((d[1:] - d[:-1]) ** 2)
        dy = torch.mean((d[:,1:] - d[:,:-1]) ** 2)
        dz = torch.mean((d[:,:,1:] - d[:,:,:-1]) ** 2)

        return dx + dy + dz

    def generate_sinus(self):
        print("I'm destroying my current values to create a very simple wave accross time, don't mind me. Well. Do mind me if this was not your intent.")
        self.generate_sampling()
        self.param = self.param * 0
        time_offset = torch.tensor([i for i in range(self.param.shape[0])], device=device)[:, None, None]
        self.param[:, :, 0] = 0.12 * torch.sin(self.canonical_sampling[None, :, 0] * 10 + time_offset)[:, 0, :]

    def border_conditions(self):
        side = self.canonical_side
        self.param[self.decal].view(side,side,side,3)[0,:,:] = 0
        self.param[self.decal].view(side,side,side,3)[:,0,:] = 0
        self.param[self.decal].view(side,side,side,3)[:,:,0] = 0
        self.param[self.decal].view(side,side,side,3)[-1,:,:] = 0
        self.param[self.decal].view(side,side,side,3)[:,-1,:] = 0
        self.param[self.decal].view(side,side,side,3)[:,:,-1] = 0

    def true_canonical(self):
        return (self.canonical_sampling * self.size_crop) + self.new_c[None,:]

    def state_dict(self,destination=None,prefix=None, keep_vars=False):
        # This function is here to save the img sampling trained
        print("Saved a previous state", self.param.shape, torch.mean(torch.mean(torch.abs(self.param),dim=1),dim=1))
        return torch.cat((self.canonical_sampling[None, :, :], self.param), dim=0)

    def load_state_dict(self, state):
        # print(state)
        # self.canonical_sampling = torch.cat((state[0], torch.tensor([[0, 0, 0]], device=device)), dim=0)
        self.canonical_sampling = state[0]
        self.param = state[1:].detach().clone()
        self.param.requires_grad = True
        self.canonical_side = round(math.pow(self.canonical_sampling.shape[0], 1 / 3))
        print("Loaded a previous state",state.shape,torch.mean(torch.mean(torch.abs(state[1:]),dim=1),dim=1))
        #self.param = torch.rand_like(self.param) * (torch.max(self.canonical_sampling) - torch.min(self.canonical_sampling)) / 5
        # self.img_sampling = torch.cat((self.param, torch.tensor([[-100, -100, -100]], device=device).repeat((state[1:].shape[0],1,1))), dim=1)
        self.update_minMax()

        if False:
            ps.init()
            ps_cloud = ps.register_point_cloud("Input points", self.canonical_sampling.cpu().detach().numpy())
            for i in range(5):
                ps_cloud.add_scalar_quantity("displacement_mean_" + str(i),
                                             torch.mean(self.param[i], dim=-1).cpu().detach().numpy())
            ps_cloud.add_scalar_quantity("displacement_mean_general",
                                         torch.mean(torch.mean(self.param, dim=-1), dim=0).cpu().detach().numpy())
            ps.show()

    def get_displ(self):
        return self.param
        # return self.img_sampling[:,:-1] - self.canonical_sampling[None,:-1]

    @torch.no_grad()
    def update_minMax(self):
        self.cell_size = (self.margin * 2) / self.grid_size
        self.shape_param = 8 / (self.cell_size * self.cell_size)
        #print(self.min.shape,self.shape_param.shape)

    def load_frames(self, idx, decal=0):
        self.decal = decal
        self.update_minMax()
        # print("Shapes :",idx.shape[0],self.grid_size**3,self.k)
        self.grid = torch.ones([idx.shape[0], self.grid_size ** 3, self.k], device=device, requires_grad=False)
        self.grid = self.grid * (self.canonical_sampling.shape[0]-1)
        # print("Indices shape when loading new frames in the grid :", idx.shape)
        self.awesome_fill_grid(idx.to(torch.int64))
        # self.grid = self.grid.detach()
        #self.new_c = torch.tensor([-0.9 + decal%9 * 0.2,-0.9 + decal // 9 * 0.2,0],device=device)

    def pad_grid(self):
        new_grid = torch.ones([self.grid_size, self.grid_size, self.grid_size, self.k * 27], device=device) * \
                   self.img_sampling.shape[0]
        for x in range(-1, 2):
            for y in range(-1, 2):
                for z in range(-1, 2):
                    k = ((x + 1) + (y + 1) * 3 + (z + 1) * 9) * self.k
                    new_grid[max(0, -x):self.grid_size - max(0, x), max(0, -y):self.grid_size - max(0, y),
                    max(0, -z):self.grid_size - max(0, z), k:k + self.k] = self.grid[
                                                                           max(0, x):self.grid_size - max(0, -x),
                                                                           max(0, y):self.grid_size - max(0, -y),
                                                                           max(0, z):self.grid_size - max(0, -z), :]
        self.grid = new_grid.to(torch.long)

    def get_tuple(self, pts, idx):  # For now pts is only a simple
        #print("My input points are :",torch.min(pts),torch.max(pts),"margin is :",self.margin)
        new_pos = (pts + self.margin) / (self.margin * 2) * self.grid_size
        new_pos = torch.clamp(torch.floor(new_pos).to(torch.long), 0, self.grid_size - 1)
        return new_pos

    """
    def get_tuple_simple(self, pts, idx):  # For now pts is only a simple
        new_pos = (pts - self.min[idx][:, None]) / (
                    self.max[idx][:, None] - self.min[idx][:, None]) * self.grid_size
        new_pos = torch.clamp(torch.floor(new_pos).to(torch.long), 0, self.grid_size - 1)
        return new_pos
    """

    def get_tuple_pad(self, pts, idx):
        # print("###### All the shapes ptdr")
        # print(pts.shape,self.min.shape,idx.shape,self.min[idx].shape)
        new_pos = (pts - self.min[idx][:, None]) / (
                    self.max[idx][:, None] - self.min[idx][:, None]) * self.grid_size + torch.tensor(
            [self.padding, self.padding, self.padding], device=device)[None, :]
        new_pos = torch.floor(new_pos).to(torch.long)
        return new_pos

    def get_value(self, tup):
        if str(tup) in self.grid:
            return self.grid[str(tup)]
        else:
            return []

    def add_to_grid(self, i, p):
        for i in range(self.k):
            if self.grid[p[0], p[1], p[2], i] == self.img_sampling.shape[0] - 1:
                self.grid[p[0], p[1], p[2], i] = i
                return

    def awesome_fill_grid(self, idx):
        self.img_sampling = torch.cat((self.canonical_sampling[None, :, :].detach() + self.param,
                                       torch.tensor([[-100, -100, -100]], device=device).repeat(
                                           (self.param.shape[0], 1, 1))), dim=1)
        #self.canonical_sampling_plus = torch.cat((self.canonical_sampling, torch.tensor([[-100, -100, -100]], device=device)),dim=0)
        pos = self.get_tuple(self.img_sampling[idx, :-1], idx)  # dim : batch_size x img_per_epoch x sample_size x 3

        pos = pos[:, :, 0] * self.grid_size * self.grid_size + pos[:, :, 1] * self.grid_size + pos[:, :, 2]
        if False: # todo: randomize the particle order
            random_perm = torch.rand().sort().idx 
            pos = pos[random_perm]
            new_pos, sort_idx = torch.sort(pos, stable=True)
        else:
            new_pos, sort_idx = torch.sort(pos)  # dim : batch_size x img_per_epoch x sample_size
        # From now on we have a sorted list but the position information is all flat, gotta keep that in mind
        gradient = torch.cat((torch.ones((idx.shape[0], 1), device=device), new_pos[:, 1:] - new_pos[:, :-1]),
                             dim=1).to(torch.int64)  # dim : img_per_epoch x sample_size+1
        mother_gradient = gradient.clone().to(torch.int64)  # dim : img_per_epoch x sample_size+1
        # print("Gradient is",gradient.shape)
        for i in range(self.k):
            img_id, pt_id = gradient.nonzero(as_tuple=True)
            id = sort_idx[img_id, pt_id]
            real_pos = pos[img_id, id]
            self.grid[img_id, real_pos, i] = id.to(device).to(
                torch.float32)  # THERE IS AN ISSUE RIGHT THERE WITH img_id THIS IS NOT WHAT I WANNA USE IT'S GONNA CAUSE TROUBLE !!!!!!!!!!!!
            gradient = torch.where(mother_gradient != 0, 0,
                                   torch.cat((torch.zeros((gradient.shape[0], 1), device=device, dtype=torch.int64),
                                              gradient[:, :-1]), dim=1))
        self.grid = self.grid.reshape(idx.shape[0], self.grid_size, self.grid_size, self.grid_size, self.k).to(
            torch.long)
        self.grid = torch.nn.functional.pad(self.grid, (
        0, 0, self.padding, self.padding, self.padding, self.padding, self.padding, self.padding),
                                            value=self.img_sampling.shape[0])

        """
        print("Shape",self.grid.shape)
        print(self.grid[0,12,12,12])
        ps.init()
        for x in range(12,17):
            for y in range(12,15):
                for z in range(21,23):
                    ps.register_point_cloud(str(x) + "," + str(y) + "," + str(z), self.img_sampling[self.grid[0,x,y,z]].cpu().detach().numpy())
        ps.show()


        cloud = torch.rand((10000,3)).to(device) * (self.max - self.min) + self.min
        pos = self.__call__(cloud,[],[])[:,:3]


        ps.init()
        ps_cloud = ps.register_point_cloud("Input points", cloud.cpu().detach().numpy())
        ps_cloud.add_scalar_quantity("index_pts_x", (pos[:, 0]).cpu().detach().numpy())
        ps_cloud.add_scalar_quantity("index_pts_y", (pos[:, 1]).cpu().detach().numpy())
        ps_cloud.add_scalar_quantity("index_pts_z", (pos[:, 2]).cpu().detach().numpy())
        ps_cloud.add_scalar_quantity("size", torch.sum(pos ** 2,dim=-1).cpu().detach().numpy())
        ps.show()
        """

    def are_points_within_polytope(self, input_pts):
        return torch.all(torch.abs(input_pts - self.new_c) <= self.size_crop,dim=-1)

    def forward(self, input_pts, img_idx, details=None, special_loss_return=False,
                 embedding=False):  # Latent codes are pretty useless I think but I will get them anyway, right ?
        # We are hijacking latent_codes by using them for img_idx, this could backfire, let me know if everything explodes
        

        #return input_pts[:,:3] + self.param[0,0,0]

        #print("Grid:",self.grid.shape,"img_idx bounds",torch.min(img_idx[:,0]),torch.max(img_idx[:,0]),self.decal)

        input_pts = (input_pts[:, :3] - self.new_c[None,:]) / self.size_crop
        # Use this next line to show which part of the scene is covered by the movement
        #input_pts[torch.max(torch.abs(input_pts),dim=-1)[0] >= 1] *= 0

        img_idx = img_idx[:, 0].to(torch.long)

        cell_idx = self.get_neighbours(input_pts, img_idx - self.decal).detach()

        #self.img_sampling =

        if details is None and special_loss_return:
            details = {}

        #weights = torch.ones(input_pts.shape[0]).to(device) * self.eps
        offsets = torch.zeros_like(input_pts).to(device)

        big_idx = (cell_idx.view((-1)) + self.param.shape[1] * (img_idx[:,None].expand(-1,self.k*8)).reshape((-1)) ).detach()

        cell_pts = torch.gather((self.canonical_sampling[None, :, :].detach() + self.param).reshape((-1,3)), 0, big_idx[:,None].expand(-1,3)).view((-1,self.k*8,3))
        #cell_pts = torch.gather((self.img_sampling).reshape((-1, 3)), 0, big_idx[:, None].expand(-1, 3)).view((-1, self.k * 8, 3))

        #print(big_idx.shape,cell_pts.shape,cell_pts.view((-1,3)).shape,input_pts.shape)
        #print("Is it the same ???",cell_pts[0],(self.canonical_sampling[None, :, :].detach() + self.param)[img_idx[0]][cell_idx[0]])

        #return input_pts + cell_pts[:,0]

        #dist = torch.cdist(input_pts[:,None,:],cell_pts,2)[:,0] ** 2
        dist = torch.sum((cell_pts - input_pts[:, None, :].detach()) ** 2, dim=-1)
        dist = torch.exp(-1 * self.shape_param * dist)
        #s = self.cell_size * 0.5
        #dist = torch.where(dist < s*s,torch.exp(- s*s / (s*s - dist)), torch.zeros_like(dist))
        #weights = torch.ones_like(dist[...,0])

        weights = torch.sum(dist, dim=-1) + self.eps
        offsets += torch.sum((self.canonical_sampling[cell_idx].detach() - cell_pts) * dist[:, :, None], dim=-2)

        if False:
            ps.init()
            ps_cloud = ps.register_point_cloud("Input points", input_pts.detach().cpu().numpy())
            part = ps.register_point_cloud("Bended particles",(self.canonical_sampling + self.param[0]).detach().cpu().numpy())
            part.add_scalar_quantity("Total deformation",torch.mean(torch.abs(self.param[0]),dim=1).detach().cpu().numpy())
            ps.show()

        # print("Offset :",torch.mean(offsets / weights[:,None]),torch.max(self.param))
        # Let's give accuracy scores to the points in the scene
        offsets = offsets / weights[:,None]
        if self.accuracy_compute:  # WARNING !!! THIS is written with rendering in mind. IT WON'T WORK AS INTENDED FOR TRAINING
            print("I'm accuracy scoring !")
            side = self.canonical_side
            d = self.param[self.decal].reshape(side, side, side, 3)
            c = self.canonical_sampling.reshape(side,side,side,3)
            popos = torch.floor(((input_pts + offsets) / 2 + 0.5) * (side)).to(torch.int64) # Super not precise but eh
            sum_dist = torch.zeros((popos.shape[0]),device=device)
            sum_d = torch.zeros((popos.shape[0],3),device=device)
            for x in range(2):
                for y in range(2):
                    for z in range(2):
                        curpos = torch.clamp(popos + torch.tensor([x,y,z],device=device)[None,:],0,side-1).to(torch.int64)
                        dist = torch.sum((input_pts + offsets - c[curpos[:,0],curpos[:,1],curpos[:,2]])**2,dim=-1)
                        sum_dist = sum_dist + dist
                        sum_d = sum_d + dist[:,None] * d[curpos[:,0],curpos[:,1],curpos[:,2]]

            sum_d = sum_d / sum_dist[:,None]
            #print("All of the shapes and more :",popos.shape,d.shape,offsets.shape)
            #print(d[popos[:,0],popos[:,1],popos[:,2]].shape)
            #print("Heyo :",self.canonical_sampling[0],self.canonical_sampling[-1])
            self.accuracy_score = torch.sum((offsets + sum_d)**2,dim=-1)


        input_pts += offsets



        if embedding:
            return self.embed_fn(input_pts)
        else:
            return (input_pts * self.size_crop) + self.new_c[None,:]


    def get_neighbours(self, input_pts, img_idx):

        # input_pts = input_pts[:, :3]   # Input pts have a regular size of    pts_size x 3
        # Meanwhile img_idx is    pts_size

        # return torch.ones((input_pts.shape[0],10)).to(torch.long)

        #index_pts = self.get_tuple_pad(input_pts, img_idx)
        # print("index pts shape", index_pts.shape)

        """
        print("Here is what I, the rbf renderer, get as input", input_pts)
        print("Resulting index pts", index_pts)
        print("And here is the extent of my sampling", self.min, self.max)
        print("And here is the latent code I recieve", img_idx)
        print("Of course there are a bunch of shapes", input_pts.shape, img_idx.shape, self.grid.shape,
              self.img_sampling.shape,self.min.shape)
        #raise "Everything's wrong ! I don't know how but I know it anyway. Peace !"
        """



        bb = 1
        idx_list = []
        for x in range(-1,2,2):
            for y in range(-1,2,2):
                for z in range(-1,2,2):
                    #print(input_pts.shape,self.cell_size.shape,img_idx.shape)
                    current_index = self.get_tuple(input_pts + (torch.tensor([x,y,z],device=device) * 0.49999 )[None,:] * self.cell_size , img_idx)
                    #print("Before indexing",torch.max(current_index))
                    cell_idx = self.grid[img_idx, current_index[:, 0], current_index[:, 1], current_index[:, 2]]
                    idx_list.append(cell_idx)
        """
        for x in range(-bb, 1 + bb):
            for y in range(-bb, 1 + bb):
                for z in range(-bb, 1 + bb):
                    current_index = index_pts + torch.tensor([x, y, z])[None, :].to(device)
                    current_index = torch.clamp(current_index, 0, self.grid_size - 1 + 2 * self.padding)
                    # print("index shape",current_index.shape)
                    cell_idx = self.grid[img_idx, current_index[:, 0], current_index[:, 1], current_index[:, 2]]
                    #print(cell_idx.shape)
                    idx_list.append(cell_idx)
        """

        return torch.cat(idx_list,1)


    def get_closest(self,pts,idx):
        idx = idx[:, 0].to(torch.long)
        neigh_idx = self.get_neighbours(pts,idx)

        neigh_pts = (self.canonical_sampling[None] + self.param)[idx[:,None],neigh_idx] # Dim : pts.shape[0], self.k*8, 3
        dist = torch.sum((neigh_pts - pts[:, None, :].detach()) ** 2, dim=-1)
        #print(dist.shape,neigh_pts.shape,pts.shape)
        v,i = torch.min(dist,dim=1)
        # i is size pts.shape[0] ???

        return v, neigh_idx[i]




class My_Multi_Accurate_Bender():

    def __init__(self, embed_fn, canonical_sampling, img_sampling, grid_size=32, k=10, radius=0.01):
        # img_sampling : nImg x sample_size x 3

        self.param = img_sampling
        self.canonical_sampling = canonical_sampling.to(device)

        self.radius = radius
        # print("Min, max", self.min, self.max, "epsilon:", ((self.max - self.min) / grid_size) ** 2)
        self.grid = {}
        self.eps = 0.000000001
        self.decal = 0

    def generate_sampling(self, min, max):
        print(
            "Let's be clear, this function should only be used at the beginning of training, if you see this sentence anywhere else, something has gone wrong.")
        self.canonical_sampling = torch.rand_like(self.canonical_sampling) * (max[None, :] - min[None, :]) + min[None,:]


    def generate_sinus(self):
        print("Generate sinus doesn't work anymore")
        return
        print("I'm destroying my current values to create a very simple wave accross time, don't mind me. Well. Do mind me if this was not your intent.")
        self.img_sampling = self.img_sampling * 0
        time_offset = torch.tensor([i for i in range(self.img_sampling.shape[0])], device=device)[:, None, None]
        self.img_sampling[:, :, 0] = 0.5 * torch.sin(self.canonical_sampling[None, :, 0] + time_offset)[:, 0, :]
        self.img_sampling = self.canonical_sampling[None, :, :] + self.img_sampling

    def state_dict(self):
        # This function is here to save the img sampling trained
        return torch.cat((self.canonical_sampling[None, :, :], self.param), dim=0)

    def load_state_dict(self, state):
        # print(state)
        # self.canonical_sampling = torch.cat((state[0], torch.tensor([[0, 0, 0]], device=device)), dim=0)
        self.canonical_sampling = state[0]
        self.param = state[1:] * 100
        self.param = torch.rand_like(self.param) * (
                    torch.max(self.canonical_sampling) - torch.min(self.canonical_sampling)) / 5
        # self.img_sampling = torch.cat((self.param, torch.tensor([[-100, -100, -100]], device=device).repeat((state[1:].shape[0],1,1))), dim=1)
        self.update_minMax()

        if False:
            ps.init()
            ps_cloud = ps.register_point_cloud("Input points", self.canonical_sampling.cpu().detach().numpy())
            for i in range(5):
                ps_cloud.add_scalar_quantity("displacement_mean_" + str(i),
                                             torch.mean(self.param[i], dim=-1).cpu().detach().numpy())
            ps_cloud.add_scalar_quantity("displacement_mean_general",
                                         torch.mean(torch.mean(self.param, dim=-1), dim=0).cpu().detach().numpy())
            ps.show()

    def get_displ(self):
        return self.param
        # return self.img_sampling[:,:-1] - self.canonical_sampling[None,:-1]

    def update_minMax(self):
        self.min, _ = torch.min(self.canonical_sampling[None, :] + self.param, 1)
        self.max, _ = torch.max(self.canonical_sampling[None, :] + self.param, 1)
        self.min, _ = torch.min(self.min, 1)
        self.max, _ = torch.max(self.max, 1)
        self.shape_param = (self.radius * self.grid_size / (self.max - self.min)) ** 2

    def load_frames(self, idx,d=0):
        self.decal = d
        self.grid = self.param[idx.to(torch.int64)]
        # print("Indices shape when loading new frames in the grid :", idx.shape)
        #self.awesome_fill_grid(idx.to(torch.int64))

    def pad_grid(self):
        new_grid = torch.ones([self.grid_size, self.grid_size, self.grid_size, self.k * 27], device=device) * \
                   self.img_sampling.shape[0]
        for x in range(-1, 2):
            for y in range(-1, 2):
                for z in range(-1, 2):
                    k = ((x + 1) + (y + 1) * 3 + (z + 1) * 9) * self.k
                    new_grid[max(0, -x):self.grid_size - max(0, x), max(0, -y):self.grid_size - max(0, y),
                    max(0, -z):self.grid_size - max(0, z), k:k + self.k] = self.grid[
                                                                           max(0, x):self.grid_size - max(0, -x),
                                                                           max(0, y):self.grid_size - max(0, -y),
                                                                           max(0, z):self.grid_size - max(0, -z), :]
        self.grid = new_grid.to(torch.long)

    def get_tuple(self, pts, idx):  # For now pts is only a simple
        new_pos = (pts - self.min[idx][:, None, None]) / (
                    self.max[idx][:, None, None] - self.min[idx][:, None, None]) * self.grid_size
        new_pos = torch.clamp(torch.floor(new_pos).to(torch.long), 0, self.grid_size - 1)
        return new_pos

    def get_tuple_pad(self, pts, idx):
        # print("###### All the shapes ptdr")
        # print(pts.shape,self.min.shape,idx.shape,self.min[idx].shape)
        new_pos = (pts - self.min[idx][:, None]) / (
                    self.max[idx][:, None] - self.min[idx][:, None]) * self.grid_size + torch.tensor(
            [self.padding, self.padding, self.padding], device=device)[None, :]
        new_pos = torch.floor(new_pos).to(torch.long)
        return new_pos

    def get_value(self, tup):
        if str(tup) in self.grid:
            return self.grid[str(tup)]
        else:
            return []

    def add_to_grid(self, i, p):
        for i in range(self.k):
            if self.grid[p[0], p[1], p[2], i] == self.img_sampling.shape[0] - 1:
                self.grid[p[0], p[1], p[2], i] = i
                return

    def awesome_fill_grid(self, idx):
        self.img_sampling = torch.cat((self.canonical_sampling[None, :, :] + self.param,
                                       torch.tensor([[-100, -100, -100]], device=device).repeat(
                                           (self.param.shape[0], 1, 1))), dim=1)
        pos = self.get_tuple(self.img_sampling[idx, :-1], idx)  # dim : img_per_epoch x sample_size x 3

        pos = pos[:, :, 0] * self.grid_size * self.grid_size + pos[:, :, 1] * self.grid_size + pos[:, :, 2]
        new_pos, sort_idx = torch.sort(pos)  # dim : img_per_epoch x sample_size
        # From now on we have a sorted list but the position information is all flat, gotta keep that in mind
        gradient = torch.cat((torch.ones((idx.shape[0], 1), device=device), new_pos[:, 1:] - new_pos[:, :-1]),
                             dim=1).to(torch.int64)  # dim : img_per_epoch x sample_size+1
        mother_gradient = gradient.clone().to(torch.int64)  # dim : img_per_epoch x sample_size+1
        # print("Gradient is",gradient.shape)
        for i in range(self.k):
            img_id, pt_id = gradient.nonzero(as_tuple=True)
            id = sort_idx[img_id, pt_id]
            real_pos = pos[img_id, id]
            self.grid[img_id, real_pos, i] = id.to(device).to(
                torch.float32)  # THERE IS AN ISSUE RIGHT THERE WITH img_id THIS IS NOT WHAT I WANNA USE IT'S GONNA CAUSE TROUBLE !!!!!!!!!!!!
            gradient = torch.where(mother_gradient != 0, 0,
                                   torch.cat((torch.zeros((gradient.shape[0], 1), device=device, dtype=torch.int64),
                                              gradient[:, :-1]), dim=1))
        self.grid = self.grid.reshape(idx.shape[0], self.grid_size, self.grid_size, self.grid_size, self.k).to(
            torch.long)
        self.grid = torch.nn.functional.pad(self.grid, (
        0, 0, self.padding, self.padding, self.padding, self.padding, self.padding, self.padding),
                                            value=self.img_sampling.shape[0])

        """
        print("Shape",self.grid.shape)
        print(self.grid[0,12,12,12])
        ps.init()
        for x in range(12,17):
            for y in range(12,15):
                for z in range(21,23):
                    ps.register_point_cloud(str(x) + "," + str(y) + "," + str(z), self.img_sampling[self.grid[0,x,y,z]].cpu().detach().numpy())
        ps.show()


        cloud = torch.rand((10000,3)).to(device) * (self.max - self.min) + self.min
        pos = self.__call__(cloud,[],[])[:,:3]


        ps.init()
        ps_cloud = ps.register_point_cloud("Input points", cloud.cpu().detach().numpy())
        ps_cloud.add_scalar_quantity("index_pts_x", (pos[:, 0]).cpu().detach().numpy())
        ps_cloud.add_scalar_quantity("index_pts_y", (pos[:, 1]).cpu().detach().numpy())
        ps_cloud.add_scalar_quantity("index_pts_z", (pos[:, 2]).cpu().detach().numpy())
        ps_cloud.add_scalar_quantity("size", torch.sum(pos ** 2,dim=-1).cpu().detach().numpy())
        ps.show()
        """

    def __call__(self, input_pts, img_idx, details=None, special_loss_return=False,
                 embedding=False):  # Latent codes are pretty useless I think but I will get them anyway, right ?
        # We are hijacking latent_codes by using them for img_idx, this could backfire, let me know if everything explodes

        #return input_pts + self.param[:1,:1]

        img_idx = img_idx[:, 0].to(torch.long)

        canonical = self.canonical_sampling[None,:,:] + self.param


        if details is None and special_loss_return:
            details = {}

        input_pts = input_pts[:, :3]  # Input pts have a regular size of    pts_size x 3

        #return torch.zeros_like(input_pts) * self.param[0,0,0]

        weights = torch.ones(input_pts.shape[0]).to(device) * self.eps
        offsets = torch.zeros_like(input_pts).to(device)

        dist = torch.sum((canonical - input_pts[:, None, :]) ** 2, dim=-1)
        dist = torch.exp(-1 * self.radius * dist)
        weights += torch.sum(dist, dim=-1)
        offsets += torch.sum((-self.param[img_idx]) * dist[:, :, None], dim=-2)

        if details is not None:
            details["unmasked_offsets"] = offsets / weights[:, None]
            details["masked_offsets"] = details["unmasked_offsets"]
            # print("Details shape :",details["masked_offsets"].shape)
            details["rigidity_mask"] = torch.ones((details["masked_offsets"].shape[0], 1), device=device)

        if special_loss_return:  # used for compute_divergence_loss()
            return details

        #print("Offset :",torch.mean(offsets / weights[:,None]),torch.max(self.param),(offsets/weights[:,None]).shape,input_pts.shape)
        new_pts = input_pts + offsets / weights[:, None]
        if embedding:
            return self.embed_fn(new_pts)
        else:
            return new_pts

def render_image(args, poses, intrinsics, ray_bending_latents,canonical_sampling,render_kwargs_test,simulation,truc=2):

    poses = torch.Tensor(poses).cuda().reshape(-1, 3, 4)

    # This is useless (not so much)
    #ray_bending_multiplied = torch.cat([raybending_latents[0] for _ in range(len(canonical_sampling))])
    if args.reconstruction:
        latent = ray_bending_latents.expand( canonical_sampling.shape[0], -1)

    # This is useful !
    #img_sampling,canonical_sampling = canonical_sampling,img_sampling
    #img_sampling = render_kwargs_test["debender"](canonical_sampling,ray_bending_multiplied)
    #print(simulation.get_disp()[900000])

    #rperm = torch.randperm(canonical_sampling.shape[0])[:truc]

    if args.reconstruction:
        img_sampling = render_kwargs_test["ray_bender"](canonical_sampling.to(device), latent.to(device), None)[:,:3]
        #img_sampling = canonical_sampling.clone()
    else:
        img_sampling = canonical_sampling + simulation.get_disp(canonical_sampling) * 0.4

    #img_sampling = canonical_sampling + torch.sin(canonical_sampling * 2) * 0.04
    #img_sampling = canonical_sampling + torch.sin(canonical_sampling * 12.0) * 0.03

    embed_fn = render_kwargs_test["ray_bender"].embed_fn

    coarse_model = render_kwargs_test["network_fn"]
    fine_model = render_kwargs_test["network_fine"]
    #grid_size = 16 + truc
    #cell_size = 24 - truc // 4
    ray_bender = My_Padded_Bender(embed_fn, canonical_sampling, img_sampling,32,13,1.5)
    coarse_model.ray_bender = (ray_bender,)
    fine_model.ray_bender = (ray_bender,)

    parallel_render = train.get_parallelized_render_function(
        coarse_model=coarse_model, fine_model=fine_model, ray_bender=ray_bender, ray_debender=None
    )

    with torch.no_grad():
        returned_outputs = train.render_path(
            poses,
            intrinsics,
            args.chunk,
            render_kwargs_test,
            render_factor=2,
            detailed_output=False,
            ray_bending_latents=ray_bending_latents,
            parallelized_render_function=parallel_render,
            displacement=False
        )

    rgbs, disps = returned_outputs
    return rgbs[0]

def project(u,pos,intrin):
    target = np.linalg.inv(pos[:3, :3]).dot(u - pos[:3, -1])
    target = target / (- target[2])
    x, y = target[0] * intrin["focal_x"] + intrin["center_x"], -target[1] * intrin["focal_y"] + intrin["center_y"]
    x, y = int(x), int(y)
    return x,y

def draw_pixel(img,x,y,t):
    x,y = int(x), int(y)
    if x >= 0 and x < img.shape[1] and y >= 0 and y < img.shape[0]:
        img[y][x] *= t
    return img


def draw_line(img,xa,ya,xb,yb):
    xb,yb = xb-xa,yb-ya
    t = 0
    step = 0.5 / (abs(xb) + abs(yb))
    while t <= 1:
        img = draw_pixel(img,xa + t*xb,ya + t*yb, t * 0.5)
        t += step

    return img

def render_arrow(pos,intrin,base_pt,force,img):
    force = np.array(force)
    base_x, base_y = project(base_pt,pos,intrin)
    point_x, point_y = project(base_pt + force * 0.01,pos,intrin)
    """
    for i in range(x-2,x+3):
        for j in range(y-2,y+3):
            if i >= 0 and i < img.shape[1] and j >= 0 and j < img.shape[0]:
                img[j][i] *= 0
    """
    draw_line(img,base_x,base_y,point_x,point_y)
    print("A short piece of the image",img[10][10])

    return img

def main(args):

    (
        render_kwargs_train,
        render_kwargs_test,
        start,
        grad_vars,
        load_weights_into_network,
        checkpoint_dict,
        get_training_ray_bending_latents,
        load_llff_dataset,
        raw_render_path,
        render_convenient,
        convert_rgb_to_saveable,
        convert_disparity_to_saveable,
        convert_disparity_to_jet,
        convert_disparity_to_phong,
        store_ray_bending_mesh_visualization,
        to8b,
    ) = fvr._setup_nonrigid_nerf_network(args.input,checkpoint=args.checkpoint)

    (
        images,
        poses,
        all_rotations,
        all_translations,
        bds,
        render_poses,
        render_rotations,
        render_translations,
        i_train,
        i_val,
        i_test,
        near,
        far,
        extras,
    ) = load_llff_dataset(render_kwargs_train, render_kwargs_test)
    ray_bending_latents = ( get_training_ray_bending_latents(checkpoint = args.checkpoint) )
    poses = poses[i_train]
    ray_bending_latents = ray_bending_latents[i_train]
    images = images[i_train]
    intrinsics = extras["intrinsics"]

    args_logged = train.config_parser().parse_args(
        ["--config", os.path.join(args.input, "logs", "args.txt")]
    )
    path = os.path.join(args.input, args.sampling + ".tar")
    sampling = torch.load(path)
    output_folder = args.input + "/rbf_renderings/"
    rrrr = os.path.join(output_folder)

    train.create_folder(rrrr)

    if args.reconstruction:
        for i in range(ray_bending_latents.shape[0]):
            img = render_image(args, poses[i:i+1], [intrinsics[i]], ray_bending_latents[i:i+1], sampling, render_kwargs_test, None)
            imageio.imwrite(os.path.join(output_folder, "reconstruction" + str(i) + ".png"), img)
        return

    i = 1
    omegas,UVs = load_omegas(args.input)
    print(UVs.shape)
    print("#############################################################\n\n",omegas)


    #omegas,UVs,c,initial = load_continuous_omegas(args.input)
    #print("Argmax",np.argmax(np.max(np.max(np.abs(UVs),axis=-1),axis=-1)))
    sim = simulation.Simulation(omegas,UVs,step=0.01)
    def find_moving_pt(U):
        n = 0
        while U[n,0,3] == 0:
            n += 1
        return n
    n = find_moving_pt(UVs)

    if args.polyscope:
        import polyscope as ps

        ps.init()
        ps_cloud = ps.register_point_cloud("Canonical points", sampling.cpu().detach().numpy())
        ps_cloud.add_scalar_quantity("UVs value", np.mean(np.mean(UVs,axis=-1),axis=-1))
        outline = np.zeros(sampling.shape[0])
        outline[0] = 1
        ps_cloud.add_scalar_quantity("N",outline)
        ps.show()
    fleche = [-0.3 * 0, 0.25 * 10, -0.1 * 0]
    fleche2 = [-0.3 * 10, 0.25 * 0, -0.1 * 0]
    fleche3 = [-0.3 * 0, 0.25 * 0, -0.1 * 10]
    f = sim.get_force([n], fleche)
    print(f)
    sim.add_step(f)
    #sim.direct_manip([926651],[15,-21,8])

    """
    for j in range(3000):
        sim.add_step(0)

    h = sim.history
    x = [i for i in range(len(h))]
    # y = h[:,1,0]
    for i in range(10):
        plt.plot(x, h[:, 0, i])
        plt.title("Mode number " + str(i))
        plt.show()
    """

    i = 370
    j = 400
    #sim.direct_manip([926650], [52, -30, -18])
    min_pos = np.min(poses,axis=0)
    max_pos = np.max(poses,axis=0)
    for t in trange(1, 225):
        # About intrinsics : it's not handled that way in free_viewpoint_rendering.py, there must be a reason, I have to investigate this
        #print(intrinsics)
        for fssgfg in range(19):
            sim.add_step(0)
            #pass
        #i = np.abs(100 - t) + 1
        if t%100 == 0:
            i += 30
            j += 30
            sim.add_step(f)
        else:
            sim.add_step(0)
            #pass
        tau = (t % 100) / 100
        current_pose = tau * poses[j:j+1] + (1 - tau) * poses[i:i+1]
        #current_pose = poses[10:11]
        #print(current_pose)
        #print(intrinsics[i])
        #for a in range(10001,200002,10000):
            #b = min(a,sampling.shape[0])
        img = render_image(args,current_pose,[intrinsics[i]],ray_bending_latents[0:1],sampling,render_kwargs_test,sim)
        #img = render_arrow(current_pose[0],intrinsics[i],sampling[n],fleche,img)
        #img = render_arrow(current_pose[0],intrinsics[i],sampling[n],fleche2,img)
        #img = render_arrow(current_pose[0],intrinsics[i],sampling[n],fleche3,img)
        imageio.imwrite(os.path.join(output_folder, str(t) + ".png"), img)

    h = sim.history
    x = [i for i in range(len(h))]
    # y = h[:,1,0]
    for i in range(14):
        plt.plot(x, h[:, 0, i])
        plt.title("Mode number " + str(i))
        plt.show()


def load_omegas(input):
    path0 = os.path.join(args.input, "omega_real.tar")
    path1 = os.path.join(args.input, "omega_imag.tar")
    omega = torch.load(path0) + 1.j * torch.load(path1) * 0

    path0 = os.path.join(args.input, "uv_real.tar")
    path1 = os.path.join(args.input, "uv_imag.tar")
    UV = torch.load(path0) + 1.j * torch.load(path1)
    #UV[:,:,0] = 0
    return omega, UV

def load_continuous_omegas(input):
    path = os.path.join(args.input, "modes/", "omegas.tar")
    omega = torch.load(path).cpu().detach()

    path = os.path.join(args.input, "modes/", "dampness.tar")
    dampness = torch.load(path).cpu().detach()

    path = os.path.join(args.input, "modes/", "initial.tar")
    initial = torch.load(path).cpu().detach()

    path = os.path.join(args.input, "modes/", "modes.tar")
    UV_param = torch.load(path)

    UV = [train_modes.Mode() for _ in range(len(UV_param))]
    for i in range(len(UV)):
        UV[i].load_state_dict(UV_param[i])

    return np.array(omega), UV, np.array(dampness), np.array(initial)

if __name__ == "__main__":

    import configargparse

    parser = configargparse.ArgumentParser()
    # mandatory arguments
    parser.add_argument(
        "--input",
        type=str,
        help="the experiment folder that was created by train.py when training the network.",
    )
    parser.add_argument(
        "--checkpoint",
        type=str,
        default="latest",
        help="the experiment folder that was created by train.py when training the network.",
    )
    parser.add_argument(
        "--sampling",
        type=str,
        default="canonic_sampling",
        help="name of the sampling file to use."
    )
    parser.add_argument(
        "--N_sample",
        type=int,
        default=64,
        help="number of points sampled accross each rays.",
    )
    parser.add_argument(
        "--batch_size",
        type=int,
        default=64000,
        help="",
    )
    parser.add_argument(
        "--N_pts",
        type=int,
        default=1000,
        help="number of points sampled in the final space.",
    )
    parser.add_argument(
        "--chunk",
        type=int,
        default=1024 * 32,
        help="number of rays processed in parallel, decrease if running out of memory",
    )
    parser.add_argument(
        "--reconstruction",
        action="store_true",
        help="activate in order to test out the quality of the debender"
    )
    parser.add_argument(
        "--polyscope",
        action="store_true",
        help="activate the visualisation of the modes over all points."
    )

    args = parser.parse_args()

    main(args)