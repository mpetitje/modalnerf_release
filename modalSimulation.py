import torch
from scipy.fft import fft, fftfreq

import numpy as np
from matplotlib import pyplot as plt

device = torch.device("cuda" if torch.cuda.is_available() else "cpu")

class Simulation:
  def __init__(self,omega,VUs,step=0.002):
    # omega is N, VUs is k, 3, N, all complex
    self.load_omUv(omega,VUs)
    self.step = step

  def load_omUv(self,omega,VUs,base_move=None):
    self.omega = omega
    self.VUs = VUs
    self.q = torch.zeros((omega.shape[0]),dtype=torch.cfloat)
    self.s = torch.zeros((omega.shape[0]),dtype=torch.cfloat)
    self.c = torch.ones((omega.shape[0])) * 0.9
    if base_move == None:
        self.base_move = torch.zeros((self.VUs.shape[0],3))
    else:
        self.base_move = base_move

  def set_mode(self,f):
      self.q = self.q*0 + f * self.step
      self.s = self.s * 0

  def get_displ_old(self):
    return torch.sum(self.VUs * self.q[None,None,:],dim=-1).real

  def get_displ(self):
    #print("Here is everything about my displ")
    #print(self.omega)
    #print(self.q)
    return torch.sum(self.VUs * (self.q - 1.j*self.s/self.omega)[None,None,:],dim=-1).real \
           + self.base_move

  def get_axis_displ(self):
    V = self.VUs.clone()
    for i in range(3):
      if self.q[i] == 0:
        V[:,i,:] *= 0
    return torch.sum(V * (self.q - 1.j*self.s/self.omega)[None,None,:],dim=-1).real

  def modal_analysis(self,pts, weight = None):
    # pts have k, t, 3 shape
    show_sphere = False

    pts = pts.numpy()
    timeline_x = fft(pts[:,:,0])
    timeline_y = fft(pts[:,:,1])
    timeline_z = fft(pts[:,:,2])
    if show_sphere:
        timeline_s = np.sin(np.linspace(0,2*np.pi * 4,100)) * 0.0002
        timeline_s = fft(timeline_s)
    #print(timeline_y)

    bb_move = np.concatenate([timeline_x[:,0,np.newaxis], timeline_y[:,0,np.newaxis], timeline_z[:,0,np.newaxis]],axis=-1) / pts.shape[1]
    #print("My fantastic bb_move :",bb_move,np.max(bb_move))
    self.base_move = torch.tensor(bb_move).real

    realFreq = fftfreq(pts.shape[1],self.step)

    n_frames = pts.shape[1]
    x = np.arange(1, n_frames // 2)
    if weight is not None:
        weight = weight.numpy()
        print(weight)
        #weight[weight < 1.1] *= 0
        tt = np.ones_like(weight)
        tt[weight < 0.4] *= 0
        plotable = np.mean(np.abs(np.real(timeline_x + timeline_y + timeline_z) * tt[:,np.newaxis])[:,1:n_frames//2],axis=0)

    else:
        plotable = np.mean(np.abs(np.real(timeline_x + timeline_y + timeline_z))[:,1:n_frames//2],axis=0)

    timeline = np.concatenate([timeline_x[:,:,np.newaxis],timeline_y[:,:,np.newaxis],timeline_z[:,:,np.newaxis]],axis=-1)
    plotable[0] = 0

    omegas = []
    UVs = []
    freq = 1
    plt.plot(x, plotable)
    if show_sphere:
        plt.plot(x, np.abs(timeline_s[1:n_frames//2]))
    plt.show()
    while freq >= 1 and freq < n_frames:

        freq = int(input("frequency (range 1 to " + str(n_frames) + ") ? "))
        if not (freq >= 1 and freq < n_frames):
            break
        if len(omegas) == 0:
            omegas.append(realFreq[freq])
            UVs = np.transpose(timeline[:, freq:freq+1, :],(0,2,1))
            print(UVs.shape)
        elif not realFreq[freq] in omegas:
            omegas.append(realFreq[freq])
            UVs = np.concatenate([UVs, np.transpose(timeline[:, freq:freq+1, :],(0,2,1))], axis=2)
        else:
            print("Already taken, you fool !")
        print("ShApES",len(omegas),UVs.shape)

    self.load_omUv(torch.tensor(omegas,dtype = torch.cfloat),torch.tensor(UVs,dtype = torch.cfloat), self.base_move)
    print("My base move is of size",self.base_move.shape,"and is equal to :",self.base_move)

  def save(self,path):
    torch.save(self.omega,path + "/modal_omega.th")
    torch.save(self.VUs, path + "/modal_VUs.th")
    torch.save(self.base_move, path + "/base_move.th")

  def load(self,path):
    omega = torch.load(path + "/modal_omega.th")
    VUs = torch.load(path + "/modal_VUs.th")
    try:
        base_move = torch.load(path + "/base_move.th")
    except:
        base_move = None
    #print("Loading VUs,\n",VUs)
    self.load_omUv(omega,VUs,base_move)

  def add_step(self,f):
    # f is of shape N
    f = self.step * f + self.s
    self.s = f - self.q * (self.step * (self.omega * self.omega + self.c * self.c / 4))
    self.q = f * self.step + (1 + self.c * self.step) * self.q
    new_div = 1 / (1 + self.step * self.c + self.step * self.step * (self.omega * self.omega + self.c * self.c / 4))
    self.s = self.s * new_div
    self.q = self.q * new_div

    #print(self.q)

  def direct_movement(self,id,move):
    # sets the modes so that particle id is at the exact deformation move
    if self.omega.shape[0] < 3:
      print("There are not enough degrees of freedom in this scene for a free 3D movement of a point")
      print("Right now this is not implemented. Sorry. Later we can maybe do it.")
    else:
      # Possibly we could add at some point the possibility of having the speed factor in this ? With a slider it would be interesting.
      # Also it could be useful in cases where we only have 2 degrees of freedom but not when we have only 1. Still. Interesting thing to consider.
      invertedMatrix = torch.linalg.pinv(self.VUs[id,:,:3].real)
      result = torch.sum(invertedMatrix * move,dim=-1)
      self.q *= 0
      self.q[:3] = result
      self.s *= 0           # Could be that here I put a little punch for when the thing is released ? Who knows ?
      print(self.get_displ()[id],move)
      print("My freq",self.omega)
      print("My chosen modes",self.q)

  def direct_manip(self,id,dir,alpha = 1):
    #dir = np.array(dir)
    f = dir[:,None] * self.VUs[id]
    magn = torch.abs(f)
    angl = torch.angle(f)
    f = np.exp(-1.0j*angl) * magn * alpha
    self.q = f.real
    self.s = - self.omega * f.imag

  def better_direct_movement(self,id,move):
    if self.omega.shape[0] < 3:
      print("Not enough degrees of freedom.")
      return
    torch.set_grad_enabled(True)
    q = torch.ones((self.q.shape[0]), requires_grad=True)
    q.requires_grad = True
    opti = torch.optim.Adam(params=[{'params':q, 'lr':0.02}], betas=(0.9, 0.999))
    weights = torch.ones_like(q)
    w = self.VUs[id].detach().real
    for i in range(6000):
      opti.zero_grad()
      loss = torch.sum(torch.abs(torch.sum(w * q, dim=-1) - move))
      #loss += torch.mean(torch.abs(q * weights)) * 0.1
      loss.backward(retain_graph=False)
      opti.step()
    self.q = q.detach()
    self.s *= 0
    torch.set_grad_enabled(False)
    print(self.get_displ()[id], move)




def filter_out(param,sample,box):
    #print(sample[0:10])
    idx = torch.max(torch.abs(sample),dim=1)[0] > torch.ones(sample.shape[0],device=device) * box
    #print(idx)
    param[idx] *= 0
    return param

def main(args):
    ckpt = torch.load(args.ckpt, map_location=device)
    state = ckpt["bender"]
    canonical_sampling = state[0].detach()
    param = state[1:].detach() # t, k, 3
    param = torch.transpose(param,1,0)

    param = filter_out(param,canonical_sampling,0.6)

    sim = Simulation(torch.zeros((1)),torch.zeros((1,3,1)))

    plots = np.mean(param.cpu().numpy(),axis=0)
    x = np.array(range(param.shape[1]))
    for i in range(3):
        plt.plot(x,plots[:,i])
    plt.show()

    print(param.shape,canonical_sampling.shape)
    sim.modal_analysis(param.cpu())


if __name__ == "__main__":
    import configargparse

    parser = configargparse.ArgumentParser()
    # mandatory arguments
    parser.add_argument(
        "--ckpt",
        type=str,
        help="The checkpoint to reload and perform the modal analysis on.",
    )

    args = parser.parse_args()

    main(args)


