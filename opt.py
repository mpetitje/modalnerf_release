import configargparse
import os 
import sys 

def config_parser(cmd=None):
    if '--ckpt' in sys.argv and '--config' not in sys.argv:
        ckpt = sys.argv[sys.argv.index('--ckpt') + 1]
        sys.argv += ['--config', f'{os.path.dirname(ckpt)}/config.txt' ]

    parser = configargparse.ArgumentParser()
    parser.add_argument('--config', is_config_file=True,
                        help='config file path')
    parser.add_argument("--expname", type=str,
                        help='experiment name', default="automatic")
    parser.add_argument("--basedir", type=str, default='./log',
                        help='where to store ckpts and logs')
    parser.add_argument("--add_timestamp", type=int, default=0,
                        help='add timestamp to dir')
    parser.add_argument("--datadir", type=str, default='./data/llff/fern',
                        help='input data directory')
    parser.add_argument("--progress_refresh_rate", type=int, default=10,
                        help='how many iterations to show psnrs or iters')

    parser.add_argument("--canonical_sampling_size", type=int,default=100000,help="the number of points in the canonical sampling.")
    parser.add_argument("--grid_size", type=int, default=40,
                        help="Length in cells of the sides of the grid")
    parser.add_argument("--cell_limit", type=int, default=8,
                        help="Max number of particles per cell of the grid. Keep it low for time and space efficiency.")
    parser.add_argument("--polytope_size", type=str, default="1", help="Size of the polytope compared to the extent of the radiance field. From 0 to 1.")
    parser.add_argument("--polytope_pos", type=str, default="0,0,0", help="Center position of the polytope relative to the tensorf box. Set of 3 floats separated by ,")
    parser.add_argument("--bbox_size", type=float, default=2.0,
                        help="Size of the box covered by the radiance field")
    parser.add_argument('--no_bending', default=False, action="store_true")

    parser.add_argument('--fast_bending', default=False, action="store_true")
    parser.add_argument('--debug_bender', default=False, action="store_true")

    parser.add_argument('--deform_t_power', default=0.0, type=float)
    parser.add_argument('--time_t_power',default=0.0, type=float)

    parser.add_argument('--highlight_polytope', default=False, action="store_true")
    parser.add_argument('--remove_viewdir_in_polytope', default=False, action="store_true")

    parser.add_argument('--with_depth', action='store_true')
    parser.add_argument('--downsample_train', type=float, default=1.0)
    parser.add_argument('--downsample_test', type=float, default=1.0)

    parser.add_argument('--model_name', type=str, default='TensorVMSplit',
                        choices=['TensorVMSplit', 'TensorCP'])

    # loader options
    parser.add_argument("--batch_size", type=int, default=4096)
    parser.add_argument("--n_iters", type=int, default=30000)

    parser.add_argument('--dataset_name', type=str, default='blender',
                        choices=['blender', 'llff', 'nsvf', 'dtu','tankstemple', 'own_data'])

    parser.add_argument('--bender_type', type=str, default='particle',
                        choices=['particle', 'MLP'])

    # training options
    # learning rate
    parser.add_argument("--lr_init", type=float, default=0.02,
                        help='learning rate')    
    parser.add_argument("--lr_basis", type=float, default=1e-3,
                        help='learning rate')
    parser.add_argument("--lr_decay_iters", type=int, default=-1,
                        help = 'number of iterations the lr will decay to the target ratio; -1 will set it to n_iters')
    parser.add_argument("--lr_decay_target_ratio", type=float, default=0.1,
                        help='the target decay ratio; after decay_iters inital lr decays to lr*ratio')
    parser.add_argument("--lr_upsample_reset", type=int, default=1,
                        help='reset lr to inital after upsampling')
    parser.add_argument("--lr_particles", type=float, default=1e-4)
    parser.add_argument('--decay_lr_particles', default=False, action="store_true")
    
    # loss
    parser.add_argument("--L1_weight_inital", type=float, default=0.0,
                        help='loss weight')
    parser.add_argument("--L1_weight_rest", type=float, default=0,
                        help='loss weight')
    parser.add_argument("--Ortho_weight", type=float, default=0.0,
                        help='loss weight')
    parser.add_argument("--TV_weight_density", type=float, default=0.0,
                        help='loss weight')
    parser.add_argument("--TV_weight_app", type=float, default=0.0,
                        help='loss weight')
    parser.add_argument("--time_continuity_weight", type=float, default=0.0,
                        help='loss weight')
    parser.add_argument("--deformation_reg_weight", type=float, default=0.0,
                        help='loss weight')
    parser.add_argument("--distortion_loss_weight", type=float, default=0.0,
                        help='loss weight')
    parser.add_argument("--total_def_weight", type=float, default=0.0,
                        help='loss weight')
    
    # model
    # volume options
    parser.add_argument("--n_lamb_sigma", type=int, action="append")
    parser.add_argument("--n_lamb_sh", type=int, action="append")
    
    parser.add_argument("--data_dim_color", type=int, default=27)

    parser.add_argument("--rm_weight_mask_thre", type=float, default=0.0001,
                        help='mask points in ray marching')
    parser.add_argument("--rm_weight_mask_thre_polytope", type=float, default=0.0001,
                        help='mask points in ray marching')
    parser.add_argument("--alpha_mask_thre", type=float, default=0.0001,
                        help='threshold for creating alpha mask volume')
    parser.add_argument("--distance_scale", type=float, default=25,
                        help='scaling sampling distance for computation')
    parser.add_argument("--density_shift", type=float, default=-10,
                        help='shift density in softplus; making density = 0  when feature == 0')
                        
    # network decoder
    parser.add_argument("--shadingMode", type=str, default="MLP_PE",
                        help='which shading mode to use')
    parser.add_argument("--pos_pe", type=int, default=6,
                        help='number of pe for pos')
    parser.add_argument("--view_pe", type=int, default=6,
                        help='number of pe for view')
    parser.add_argument("--fea_pe", type=int, default=6,
                        help='number of pe for features')
    parser.add_argument("--featureC", type=int, default=128,
                        help='hidden feature channel in MLP')
    
    parser.add_argument('--lr_comp', default=False, action="store_true")
    parser.add_argument('--sampling_size_comp', default=False, action="store_true")
    parser.add_argument('--grid_size_comp', default=False, action="store_true")
    parser.add_argument('--time_comp', default=False, action="store_true")
    parser.add_argument('--polytope_is_in_the_void', default=False, action="store_true")

    parser.add_argument('--normalize_time_reg_for_particle_count', default=False, action="store_true")

    parser.add_argument('--particle_density_multiplier',type=float,default=1.0)

    parser.add_argument("--ckpt", type=str, default=None,
                        help='specific weights npy file to reload for coarse network')
    parser.add_argument("--render_only", type=int, default=0)
    parser.add_argument("--render_test", type=int, default=0)
    parser.add_argument("--render_train", type=int, default=0)
    parser.add_argument("--render_path", type=int, default=0)
    parser.add_argument("--render_fixed", type=int, default=-1)
    parser.add_argument("--render_modal", type=int, default=-1)
    parser.add_argument("--export_mesh", type=int, default=0)
    parser.add_argument("--render_def",type=int, default=0)
    parser.add_argument("--load_modes",type=int, default=0)
    parser.add_argument("--mode_script", type=str, default="Nope", help='the script to use for the synthesis')

    parser.add_argument("--polytope",type=int, default=0)

    # rendering options
    parser.add_argument('--lindisp', default=False, action="store_true",
                        help='use disparity depth sampling')
    parser.add_argument("--perturb", type=float, default=1.,
                        help='set to 0. for no jitter, 1. for jitter')
    parser.add_argument("--accumulate_decay", type=float, default=0.998)
    parser.add_argument("--fea2denseAct", type=str, default='softplus')
    parser.add_argument('--ndc_ray', type=int, default=0)
    parser.add_argument('--nSamples', type=int, default=1e6,
                        help='sample point each ray, pass 1e6 if automatic adjust')
    parser.add_argument('--step_ratio',type=float,default=0.5)


    ## blender flags
    parser.add_argument("--white_bkgd", action='store_true',
                        help='set to render synthetic data on a white bkgd (always use for dvoxels)')
    parser.add_argument('--N_voxel_init',
                        type=int,
                        default=100**3)
    parser.add_argument('--N_voxel_final',
                        type=int,
                        default=300**3)
    parser.add_argument("--upsamp_list", type=int, action="append")
    parser.add_argument("--update_AlphaMask_list", type=int, action="append")

    parser.add_argument('--idx_view',
                        type=int,
                        default=0)

    # logging/saving options
    parser.add_argument("--N_vis", type=int, default=5,
                        help='N images to vis')
    parser.add_argument("--vis_begin", type=int, default=0,
                        help='Starting frame from the dataset')
    parser.add_argument("--vis_end", type=int, default=-1,
                        help='End frame from the dataset')
    parser.add_argument("--vis_every", type=int, default=10000,
                        help='frequency of visualize the image')
    if cmd is not None:
        args = parser.parse_args(cmd)
    else:
        args = parser.parse_args()

    # Small post-processing of some arguments
    args.polytope_pos = args.polytope_pos.split(",")
    for i in range(3):
        args.polytope_pos[i] = float(args.polytope_pos[i])

    args.polytope_size = args.polytope_size.split(",")
    if len(args.polytope_size) < 3:
        for i in range(2):
            args.polytope_size.append(args.polytope_size[0])
    for i in range(3):
        args.polytope_size[i] = float(args.polytope_size[i])
    args.polytope_size = args.polytope_size[:3]
    print("############\nMy polytope size :",args.polytope_size)
    
    if args.expname == "automatic":
        import os 
        args.expname = os.path.basename(args.config).split('.')[0]

    if args.polytope_size[0] < 1.0:
        if args.lr_comp:
            print("ADJUSTING LR FOR POLYTOPE SIZE")
            args.lr_particles /= args.polytope_size[0]
        if args.sampling_size_comp:
            print("ADJUSTING SAMPLING SIZE FOR POLYTOPE SIZE")
            args.canonical_sampling_size = int(args.canonical_sampling_size * (args.polytope_size[0] * args.particle_density_multiplier)**3)
        if args.grid_size_comp:
            print("ADJUSTING GRID SIZE FOR POLYTOPE SIZE")
            args.grid_size = int(args.grid_size * args.polytope_size[0] * args.particle_density_multiplier)
        if args.time_comp:
            print("ADJUSTING TIME CONTINUITY LOSS FOR POLYTOPE SIZE")
            args.time_continuity_weight *= args.polytope_size[0]

    if args.deform_t_power != 0.0:
        assert not args.vis_end == -1
        num_frames = args.vis_end - args.vis_begin 
        args.deformation_reg_weight = (num_frames/100)**args.deform_t_power

    if args.time_t_power != 0.0:
        assert not args.vis_end == -1
        num_frames = args.vis_end - args.vis_begin 
        args.time_continuity_weight *= (num_frames/100)**args.time_t_power

    if args.normalize_time_reg_for_particle_count:
        args.time_continuity_weight *= args.canonical_sampling_size / 50_000

    return args
            