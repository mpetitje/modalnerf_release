import numpy as np
import torch
import os
import polyscope as ps
ps.set_use_prefs_file(False)

def center(pts,c2w):
    print("Centering polytope with this c2w",c2w,"of shape",c2w.shape)
    pts = pts @ c2w[:3,:3].T

    return pts

def slice_up(obj_file):
    k = []
    line = obj_file.readline()
    while line:
        cut_line = line.split(" ")
        if cut_line[0] == "v":
            for i in range(1,len(cut_line)):
                cut_line[i] = float(cut_line[i])
            k.append(("v",cut_line[1:4]))
        elif cut_line[0] == "f":
            for i in range(1, len(cut_line)):
                if "/" in cut_line[i]:
                    cut_line[i] = cut_line[i].split("/")[0]
                cut_line[i] = int(cut_line[i])

            k.append(("f",cut_line[1:4]))
            if len(cut_line) > 4:
                k.append(("f",cut_line[3:5] + [cut_line[1]]))
        line = obj_file.readline()
    return k

def open_obj(file_path):
    vertices, faces = [],[]
    file = open(file_path, "r")

    for k, v in slice_up(file):
        if k == 'v':
            vertices.append(v)
        elif k == 'f':
            faces.append(v)
    file.close()
    if not len(faces) or not len(vertices):
        return None
    pos = torch.tensor(vertices)
    face = torch.tensor(faces).to(torch.long) - 1

    return pos,face

def load_polytope(datadir, c2w, device):
    path = os.path.join(datadir + "/polytope.obj")
    pos, face = open_obj(path)
    pos = center(pos,c2w)
    pos = pos.to(device)
    face = face.to(device)
    polytope = (pos, face)
    return polytope

def resize_polytope(polytope,bbox):
    pos,face = polytope

    pos = (pos - bbox[1]) * 2.0 / (bbox[1] - bbox[0]) - 1

    return (pos,face)

def signedV(a,b,c,d):
    return torch.sum(torch.cross(b -  a, c - a) * (d - a),axis=-1)

def has_intersection(ptA,ptB,triA,triB,triC):
    plane_cut = signedV(ptA,triA,triB,triC) * signedV(ptB,triA,triB,triC) < 0
    middleSign = signedV(ptA,ptB,triB,triC)
    left_bool = signedV(ptA,ptB,triA,triB) * middleSign > 0
    right_bool = signedV(ptA,ptB,triC,triA) * middleSign > 0
    return torch.logical_and(plane_cut,torch.logical_and(left_bool,right_bool))

def substract_polytope(pts,polytope,device, invert=False):
    far_dist = torch.max(torch.max(pts,axis=0)[0] - torch.min(pts,axis=0)[0])
    far_pts = pts + torch.ones(3).to(device) * far_dist * 3
    far_pts = far_pts.to(torch.float64)
    pts = pts.to(torch.float64)
    value = torch.zeros(pts.shape[0], device=device)
    pos,face = polytope
    pos = pos.to(device)
    face = face.to(device)
    for f in face:
        triA, triB, triC = pos[f[0]].repeat(pts.shape[0],1), pos[f[1]].repeat(pts.shape[0],1), pos[f[2]].repeat(pts.shape[0],1)
        interBool = has_intersection(pts, far_pts, triA, triB, triC)
        value = torch.where(interBool,value + 1,value)
    if invert:
        return torch.where(value % 2 == 1)[0]
    else:
        return torch.where(value % 2 == 0)[0]

def show_reduced_grid(tensorf,device):
    side = 200
    grid = np.mgrid[0:side, 0:side, 0:side].transpose((1, 2, 3, 0)).reshape((-1, 3))
    pts = torch.tensor(grid,device=device).view(-1,3) * 2 / side - 1

    side = 10
    grid = np.mgrid[0:side, 0:side, 0:side].transpose((1, 2, 3, 0)).reshape((-1, 3))

    shapyShape = (torch.tensor(grid,device=device).view(-1,3) * 2 / side - 1) * tensorf.bender.size_crop + tensorf.bender.new_c[None,:]

    canon = tensorf.bender.canonical_sampling * tensorf.bender.size_crop + tensorf.bender.new_c[None,:]
    sigma_canon = tensorf.compute_densityfeature(canon)
    sigma = tensorf.feature2density(sigma_canon)

    idCanon = torch.tensor(range(canon.shape[0]))[sigma > 0.4]
    canon = canon[sigma > 0.4]
    ampl = torch.mean(torch.abs(tensorf.bender.param.detach()),dim=-1)
    ampl = torch.max(ampl,dim=0)[0][sigma > 0.4]
    #print("Yo check out this shape dude ! UwU.",ampl.shape,canon.shape)


    sigma_feature = tensorf.compute_densityfeature(pts)
    sigma = tensorf.feature2density(sigma_feature) # Hopefully n*1

    real_pts = pts[sigma > 0.75] # Previous : 0.75


    app_features = tensorf.compute_appfeature(real_pts)
    valid_rgbs = tensorf.renderModule(real_pts, torch.ones_like(real_pts)*0.57735, app_features)

    ps.init()
    pts_cloud = ps.register_point_cloud("Sampled points",real_pts.cpu().detach().numpy())
    pts_cloud.add_color_quantity("Reconstruction colors",valid_rgbs.cpu().detach().numpy())
    ps.register_point_cloud("Deformation cage",shapyShape.cpu().detach().numpy())
    particles = ps.register_point_cloud("Canonical_particles",canon.cpu().detach().numpy())
    particles.add_scalar_quantity("amplitude",ampl.cpu().numpy())
    particles.add_scalar_quantity("id",idCanon.cpu().numpy())
    ps.show()

