import os

from opt import config_parser
import train
import torch
import numpy as np

def main_function(args):

    test_scenes = [[200000,0.2,42,0.0],[200000,0.2,42,0.05],[200000,0.2,42,0.5],[200000,0.2,42,5],[200000,0.2,42,50],[200000,0.2,42,0.005]]

    for i in range(len(test_scenes)):
        args.expname = "test_suite_spatial" + str(i)
        #args.n_iters = test_scenes[i][0]
        #args.polytope_size = test_scenes[i][1]
        args.grid_size = test_scenes[i][2]
        args.deformation_reg_weight = test_scenes[i][3]

        if args.render_only and (args.render_test or args.render_path or args.render_fixed > -1):
            args.ckpt = "log/" + args.expname + "/" + args.expname + ".th"
            train.render_test(args)
        else:
            train.reconstruction(args)


if __name__ == "__main__":
    torch.set_default_dtype(torch.float32)
    torch.manual_seed(20211202)
    np.random.seed(20211202)

    args = config_parser()

    main_function(args)