import torch
from time import perf_counter
device = torch.device("cuda" if torch.cuda.is_available() else "cpu")
from rbf_render import *
import polyscope as ps
from tqdm.auto import tqdm
import polytope as pt

ps.set_use_prefs_file(False)
ps.init()

canonical_sampling = torch.rand((100000,3),device=device) * 0
img_displ = torch.ones((250,100000,3),device=device) * 0.12
img_displ[:,:,0] *= 0
img_displ[:,:,1] *= 0

def identite(x):
    return x

benderA = My_Multi_Opti_Bender(identite,canonical_sampling,img_displ)
benderA.generate_sampling()
canonical_sampling = benderA.canonical_sampling
img_displ[:,:,2] *= torch.sin(canonical_sampling[:,1] * 10)[None,:]


