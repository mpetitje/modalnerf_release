import torch,os,imageio,sys
from tqdm.auto import tqdm
from dataLoader.ray_utils import get_rays
from models.tensoRF import TensorVM, TensorCP, raw2alpha, TensorVMSplit, AlphaGridMask
from utils import *
from dataLoader.ray_utils import ndc_rays_blender
from time import perf_counter
import modalSimulation as modal
from matplotlib import pyplot as plt
import polyscope as ps
ps.set_use_prefs_file(False)


def OctreeRender_trilinear_fast(rays, tensorf, chunk=4096, N_samples=-1, ndc_ray=False, white_bg=True, is_train=False, device='cuda', bending=True, deformation=False):

    rgbs, alphas, depth_maps, weights, z_vals = [], [], [], [], []
    N_rays_all = rays.shape[0]
    i = 0
    #rays_chunk = rays[0 * chunk:(0 + 1) * chunk].to(device)

    for chunk_idx in range(N_rays_all // chunk + int(N_rays_all % chunk > 0)):
        rays_chunk = rays[chunk_idx * chunk:(chunk_idx + 1) * chunk].to(device)
        #print("Chunk",i)
        i += 1

        rgb_map, depth_map, alpha, weight, z_val = tensorf(rays_chunk, is_train=is_train, white_bg=white_bg, ndc_ray=ndc_ray, N_samples=N_samples,bending=bending, deformation=deformation)

        #print(rgb_map.shape,depth_map.shape)
        rgbs.append(rgb_map)
        depth_maps.append(depth_map)
        weights.append(weight)
        z_vals.append(z_val)
        #del rays_chunk
        #torch.cuda.empty_cache()

    return torch.cat(rgbs), None, torch.cat(depth_maps), torch.cat(weights), torch.cat(z_vals)

@torch.no_grad()
def evaluation(test_dataset,tensorf, args, renderer, savePath=None, N_vis=5, prtx='', N_samples=-1,
               white_bg=False, ndc_ray=False, compute_extra_metrics=False, device='cuda', deformation=False):
    PSNRs, rgb_maps, depth_maps = [], [], []
    ssims,l_alex,l_vgg=[],[],[]
    os.makedirs(savePath, exist_ok=True)
    os.makedirs(savePath+"/rgbd", exist_ok=True)

    try:
        tqdm._instances.clear()
    except Exception:
        pass

    near_far = test_dataset.near_far
    img_eval_interval = 1 if N_vis < 0 else max(test_dataset.all_rays.shape[0] // N_vis,1)
    idxs = list(range(0, test_dataset.all_rays.shape[0], img_eval_interval))
    for idx, samples in tqdm(enumerate(test_dataset.all_rays[0::img_eval_interval]), file=sys.stdout):

        W, H = test_dataset.img_wh
        rays = samples.view(-1,samples.shape[-1])

        if tensorf.bender is not None:
            m = int(torch.min(rays[:, 6]))
            M = int(torch.max(rays[:, 6]))
            print("Bounds for rendering",m,M)
            tensorf.bender.load_frames(torch.tensor(range(m, M + 1)), m)

        
        rgb_map, _, depth_map, _, _ = renderer(rays, tensorf, chunk=4096, N_samples=N_samples,
                                        ndc_ray=ndc_ray, white_bg = white_bg, device=device, deformation=deformation)
        rgb_map = rgb_map.clamp(0.0, 1.0)

        rgb_map, depth_map = rgb_map.reshape(H, W, 3).cpu(), depth_map.reshape(H, W).cpu()


        if deformation:
            #print("I got the deformation alright")
            depth_map, _ = visualize_depth_numpy(depth_map.numpy())
        else:
            depth_map, _ = visualize_depth_numpy(depth_map.numpy(),near_far)


        if len(test_dataset.all_rgbs):
            gt_rgb = test_dataset.all_rgbs[idxs[idx]].view(H, W, 3)
            loss = torch.mean((rgb_map - gt_rgb) ** 2)
            PSNRs.append(-10.0 * np.log(loss.item()) / np.log(10.0))

            if compute_extra_metrics:
                ssim = rgb_ssim(rgb_map, gt_rgb, 1)
                l_a = rgb_lpips(gt_rgb.numpy(), rgb_map.numpy(), 'alex', tensorf.device)
                l_v = rgb_lpips(gt_rgb.numpy(), rgb_map.numpy(), 'vgg', tensorf.device)
                ssims.append(ssim)
                l_alex.append(l_a)
                l_vgg.append(l_v)
        rgb_map = np.hstack([rgb_map.numpy(), gt_rgb.numpy()])
        rgb_map = (rgb_map * 255).astype('uint8')
        # rgb_map = np.concatenate((rgb_map, depth_map), axis=1)
        rgb_maps.append(rgb_map)
        depth_maps.append(depth_map)
        if savePath is not None:
            imageio.imwrite(f'{savePath}/{prtx}{idx:03d}.png', rgb_map)
            rgb_map = np.concatenate((rgb_map, depth_map), axis=1)
            imageio.imwrite(f'{savePath}/rgbd/{prtx}{idx:03d}.png', rgb_map)

    imageio.mimwrite(f'{savePath}/{prtx}video.mp4', np.stack(rgb_maps), fps=20, quality=8)
    imageio.mimwrite(f'{savePath}/{prtx}depthvideo.mp4', np.stack(depth_maps), fps=20, quality=8)

    if PSNRs:
        psnr = np.mean(np.asarray(PSNRs))
        if compute_extra_metrics:
            ssim = np.mean(np.asarray(ssims))
            l_a = np.mean(np.asarray(l_alex))
            l_v = np.mean(np.asarray(l_vgg))
            np.savetxt(f'{savePath}/{prtx}mean.txt', np.asarray([psnr, ssim, l_a, l_v]))
        else:
            np.savetxt(f'{savePath}/{prtx}mean.txt', np.asarray([psnr]))


    return PSNRs

@torch.no_grad()
def evaluation_path(test_dataset,tensorf, c2ws, renderer, savePath=None, N_vis=5, prtx='', N_samples=-1,
                    white_bg=False, ndc_ray=False, compute_extra_metrics=True, device='cuda'):
    PSNRs, rgb_maps, depth_maps = [], [], []
    ssims,l_alex,l_vgg=[],[],[]
    os.makedirs(savePath, exist_ok=True)
    os.makedirs(savePath+"/rgbd", exist_ok=True)

    try:
        tqdm._instances.clear()
    except Exception:
        pass

    near_far = test_dataset.near_far
    for idx, c2w in tqdm(enumerate(c2ws)):

        W, H = test_dataset.img_wh

        c2w = torch.FloatTensor(c2w)
        rays_o, rays_d = get_rays(test_dataset.directions, c2w)  # both (h*w, 3)
        if ndc_ray:
            rays_o, rays_d = ndc_rays_blender(H, W, test_dataset.focal[0], 1.0, rays_o, rays_d)
        rays = torch.cat([rays_o, rays_d], 1)  # (h*w, 6)

        rgb_map, _, depth_map, _, _ = renderer(rays, tensorf, chunk=8192, N_samples=N_samples,
                                        ndc_ray=ndc_ray, white_bg = white_bg, device=device)
        rgb_map = rgb_map.clamp(0.0, 1.0)

        rgb_map, depth_map = rgb_map.reshape(H, W, 3).cpu(), depth_map.reshape(H, W).cpu()

        depth_map, _ = visualize_depth_numpy(depth_map.numpy(),near_far)

        rgb_map = (rgb_map.numpy() * 255).astype('uint8')
        # rgb_map = np.concatenate((rgb_map, depth_map), axis=1)
        rgb_maps.append(rgb_map)
        depth_maps.append(depth_map)
        if savePath is not None:
            imageio.imwrite(f'{savePath}/{prtx}{idx:03d}.png', rgb_map)
            rgb_map = np.concatenate((rgb_map, depth_map), axis=1)
            imageio.imwrite(f'{savePath}/rgbd/{prtx}{idx:03d}.png', rgb_map)

    imageio.mimwrite(f'{savePath}/{prtx}video.mp4', np.stack(rgb_maps), fps=30, quality=8)
    imageio.mimwrite(f'{savePath}/{prtx}depthvideo.mp4', np.stack(depth_maps), fps=30, quality=8)

    if PSNRs:
        psnr = np.mean(np.asarray(PSNRs))
        if compute_extra_metrics:
            ssim = np.mean(np.asarray(ssims))
            l_a = np.mean(np.asarray(l_alex))
            l_v = np.mean(np.asarray(l_vgg))
            np.savetxt(f'{savePath}/{prtx}mean.txt', np.asarray([psnr, ssim, l_a, l_v]))
        else:
            np.savetxt(f'{savePath}/{prtx}mean.txt', np.asarray([psnr]))


    return PSNRs

@torch.no_grad()
def evaluation_fixed(test_dataset,tensorf, n_fixed, renderer, savePath=None, N_vis=5, prtx='', N_samples=-1,
                    white_bg=False, ndc_ray=False, compute_extra_metrics=True, device='cuda',deformation=False):
    PSNRs, rgb_maps, depth_maps = [], [], []
    ssims,l_alex,l_vgg=[],[],[]
    os.makedirs(savePath, exist_ok=True)
    os.makedirs(savePath+"/rgbd", exist_ok=True)

    try:
        tqdm._instances.clear()
    except Exception:
        pass

    c2w = test_dataset.poses[int(n_fixed)]

    #tensorf.bender.generate_sinus()
    if tensorf.bender is not None:
        tensorf.bender.param.requires_grad = False

    near_far = test_dataset.near_far


    for i in tqdm(range(test_dataset.nImg)):
        
        if tensorf.bender is not None:
            print("Mean of abs of param :",torch.mean(torch.abs(tensorf.bender.param[i])))

        W, H = test_dataset.img_wh

        rays_o, rays_d = get_rays(test_dataset.directions, c2w)  # both (h*w, 3)
        if ndc_ray:
            rays_o, rays_d = ndc_rays_blender(H, W, test_dataset.focal[0], 1.0, rays_o, rays_d)
        rays = torch.cat([rays_o, rays_d, torch.ones_like(rays_o)[:,0:1] * i], 1)  # (h*w, 7)

        start = perf_counter()
        if tensorf.bender is not None:
            tensorf.bender.load_frames(torch.tensor([i]),i)
        stop = perf_counter()
        print("Took",(stop - start),"seconds to fill the grid")
        start = perf_counter()
        rgb_map, _, depth_map, _, _ = renderer(rays, tensorf, chunk=2048, N_samples=N_samples,
                                        ndc_ray=ndc_ray, white_bg = white_bg, device=device, bending=True, deformation=deformation)
        stop = perf_counter()
        print("Took", (stop - start), "seconds to render the thing")
        rgb_map = rgb_map.clamp(0.0, 1.0)

        rgb_map, depth_map = rgb_map.reshape(H, W, 3).cpu(), depth_map.reshape(H, W).cpu()

        if deformation:
            #print("I got the deformation alright")
            depth_map, _ = visualize_depth_numpy(depth_map.numpy())
        else:
            depth_map, _ = visualize_depth_numpy(depth_map.numpy(),near_far)

        rgb_map = (rgb_map.numpy() * 255).astype('uint8')
        # rgb_map = np.concatenate((rgb_map, depth_map), axis=1)
        #rgb_maps.append(rgb_map)
        #depth_maps.append(depth_map)
        if savePath is not None:
            imageio.imwrite(f'{savePath}/{prtx}{i:03d}.png', rgb_map)
            rgb_map = np.concatenate((rgb_map, depth_map), axis=1)
            imageio.imwrite(f'{savePath}/rgbd/{prtx}{i:03d}.png', rgb_map)

        if i == 0:
            rgb_map, _, depth_map, _, _ = renderer(rays, tensorf, chunk=4096, N_samples=N_samples,
                                                   ndc_ray=ndc_ray, white_bg=white_bg, device=device, bending=False,
                                                   deformation=deformation)
            rgb_map = rgb_map.clamp(0.0, 1.0)

            rgb_map = rgb_map.reshape(H, W, 3).cpu()
            #depth_map, _ = visualize_depth_numpy(depth_map.numpy(), near_far)
            if savePath is not None:
                #rgb_map = np.concatenate((rgb_map, depth_map), axis=1)
                imageio.imwrite(f'{savePath}/no_bending.png', rgb_map)

    #imageio.mimwrite(f'{savePath}/{prtx}video.mp4', np.stack(rgb_maps), fps=30, quality=8)
    #imageio.mimwrite(f'{savePath}/{prtx}depthvideo.mp4', np.stack(depth_maps), fps=30, quality=8)

    return PSNRs

@torch.no_grad()
def evaluation_modal(test_dataset,tensorf, n_fixed, renderer, savePath=None, N_vis=5, prtx='', N_samples=-1,
                    white_bg=False, ndc_ray=False, compute_extra_metrics=True, device='cuda',deformation=False):
    PSNRs, rgb_maps, depth_maps = [], [], []
    ssims,l_alex,l_vgg=[],[],[]
    os.makedirs(savePath, exist_ok=True)
    os.makedirs(savePath+"/rgbd", exist_ok=True)

    try:
        tqdm._instances.clear()
    except Exception:
        pass

    c2w = test_dataset.poses[int(n_fixed)]

    #tensorf.bender.generate_sinus()
    tensorf.bender.param.requires_grad = False

    print("We will render the different modes selected by the user.")

    # Let's init the simulation part
    canonical_sampling = tensorf.bender.canonical_sampling.detach().cpu()
    param = tensorf.bender.param.detach().cpu()  # t, k, 3
    #for i in range(param.shape[0]):
    #tensorf.bender.decal = i
    #print(tensorf.bender.spatial_reg().cpu().detach())
    if False:
        polytope_size = 0.35
        polytope_pos = torch.tensor([-0.03,0.11,0.06])
        mask = torch.max(torch.abs(canonical_sampling - polytope_pos),dim=-1)[0] > polytope_size
        print(torch.sum(mask))
        param[:,mask,:] *= 0
    param = torch.transpose(param, 1, 0)
    sim = modal.Simulation(torch.zeros((1)), torch.zeros((1, 3, 1)))

    # Density computation
    sigma_feature = tensorf.compute_densityfeature(tensorf.bender.true_canonical())
    weight = tensorf.feature2density(sigma_feature)
    weight = 1. - torch.exp(-weight)


    sim.modal_analysis(param.cpu(),weight = weight.cpu())

    sim.save(savePath)
    """
    l = 5000
    x,y = [],[]
    for i in range(l):
        x.append(i)
        for i in range(5):
           sim.add_step(torch.ones(sim.omega.shape[0]))
        y.append(sim.q[0])
    plt.plot(x,y)
    plt.show()
    """
    near_far = test_dataset.near_far

    # Save the rest state which is supposed to be different than the canonical state
    if True:
        force = torch.zeros(sim.omega.shape[0])
        sim.set_mode(force)
        tensorf.bender.param[0] = sim.get_displ()
        tensorf.bender.load_frames(torch.tensor([0]), 0)
        W, H = test_dataset.img_wh

        rays_o, rays_d = get_rays(test_dataset.directions, c2w)  # both (h*w, 3)
        if ndc_ray:
            rays_o, rays_d = ndc_rays_blender(H, W, test_dataset.focal[0], 1.0, rays_o, rays_d)
        rays = torch.cat([rays_o, rays_d, torch.ones_like(rays_o)[:, 0:1] * 0], 1)  # (h*w, 7)
        rgb_map, _, depth_map, _, _ = renderer(rays, tensorf, chunk=4096, N_samples=N_samples,
                                               ndc_ray=ndc_ray, white_bg=white_bg, device=device, bending=True,
                                               deformation=deformation)
        rgb_map = rgb_map.clamp(0.0, 1.0)

        rgb_map = rgb_map.reshape(H, W, 3).cpu()
        # depth_map, _ = visualize_depth_numpy(depth_map.numpy(), near_far)
        if savePath is not None:
            # rgb_map = np.concatenate((rgb_map, depth_map), axis=1)
            imageio.imwrite(f'{savePath}/rest_state.png', rgb_map)


    for om in range(sim.omega.shape[0]):
        os.makedirs(savePath + "/mode" + str(om), exist_ok=True)
        force = torch.zeros(sim.omega.shape[0])
        sim.set_mode(force)
        force[om] = 1200 + om * 1000
        sim.add_step(force)
        for i in tqdm(range(70)):
            force = torch.zeros(sim.omega.shape[0])
            for j in range(10):
                sim.add_step(force)
            tensorf.bender.param[0] = sim.get_displ()
            #print("This is my max disp",torch.max(torch.abs(tensorf.bender.param[0])))

            """
            for j in range(3):
                if j != om:
                    tensorf.bender.param[0,:,j] *= 0
            """

            #if i == 2:
            #    print(tensorf.bender.param[0])

            W, H = test_dataset.img_wh

            rays_o, rays_d = get_rays(test_dataset.directions, c2w)  # both (h*w, 3)
            if ndc_ray:
                rays_o, rays_d = ndc_rays_blender(H, W, test_dataset.focal[0], 1.0, rays_o, rays_d)
            rays = torch.cat([rays_o, rays_d, torch.ones_like(rays_o)[:,0:1] * 0], 1)  # (h*w, 7)

            start = perf_counter()
            tensorf.bender.load_frames(torch.tensor([0]),0)
            stop = perf_counter()
            #print("Took",(stop - start),"seconds to fill the grid")
            start = perf_counter()
            rgb_map, _, depth_map, _, _ = renderer(rays, tensorf, chunk=4096, N_samples=N_samples,
                                        ndc_ray=ndc_ray, white_bg = white_bg, device=device, bending=True, deformation=deformation)
            stop = perf_counter()
            #print("Took", (stop - start), "seconds to render the thing")
            rgb_map = rgb_map.clamp(0.0, 1.0)

            rgb_map, depth_map = rgb_map.reshape(H, W, 3).cpu(), depth_map.reshape(H, W).cpu()

            #depth_map, _ = visualize_depth_numpy(depth_map.numpy(),near_far)

            rgb_map = (rgb_map.numpy() * 255).astype('uint8')
            if savePath is not None:
                imageio.imwrite(f'{savePath}/mode{str(om)}/{prtx}{i:03d}.png', rgb_map)
                #rgb_map = np.concatenate((rgb_map, depth_map), axis=1)
                #imageio.imwrite(f'{savePath}/rgbd/{prtx}{i:03d}.png', rgb_map)

            if i == 0:
                rgb_map, _, depth_map, _, _ = renderer(rays, tensorf, chunk=4096, N_samples=N_samples,
                                                   ndc_ray=ndc_ray, white_bg=white_bg, device=device, bending=False,
                                                   deformation=deformation)
                rgb_map = rgb_map.clamp(0.0, 1.0)

                rgb_map = rgb_map.reshape(H, W, 3).cpu()
                #depth_map, _ = visualize_depth_numpy(depth_map.numpy(), near_far)
                if savePath is not None:
                    #rgb_map = np.concatenate((rgb_map, depth_map), axis=1)
                    imageio.imwrite(f'{savePath}/no_bending.png', rgb_map)

    return PSNRs

@torch.no_grad()
def interaction_modal(test_dataset,tensorf, n_fixed, renderer, savePath=None, N_vis=5, prtx='', N_samples=-1,
                    white_bg=False, ndc_ray=False, compute_extra_metrics=True, device='cuda',deformation=False):
    PSNRs, rgb_maps, depth_maps = [], [], []
    ssims,l_alex,l_vgg=[],[],[]
    os.makedirs(savePath, exist_ok=True)
    os.makedirs(savePath+"/rgbd", exist_ok=True)

    try:
        tqdm._instances.clear()
    except Exception:
        pass

    c2w = test_dataset.poses[int(n_fixed)]

    #tensorf.bender.generate_sinus()
    tensorf.bender.param.requires_grad = False

    print("We will render the different modes selected by the user.")
    # Let's init the simulation part
    canonical_sampling = tensorf.bender.canonical_sampling.detach()
    param = tensorf.bender.param.detach()  # t, k, 3
    param = torch.transpose(param, 1, 0)
    sim = modal.Simulation(torch.zeros((1)), torch.zeros((1, 3, 1)))

    sim.load(savePath)

    #sim.better_direct_movement(50000, torch.zeros(3))
    near_far = test_dataset.near_far

    om = 19
    while True:
        om += 1
        os.makedirs(savePath + "/interaction" + str(om), exist_ok=True)
        force = torch.zeros(sim.omega.shape[0])
        sim.set_mode(force)
        print(sim.omega)
        if input("Want to apply forces ? (Y/N)").lower() in ["y","yes","oui"]:
            forceInit = torch.zeros(sim.omega.shape[0])
            for i in range(force.shape[0]):
                forceInit[i] = float(input("What force on mode"+str(i)))
            sim.add_step(forceInit)
            k = 0
        else:
            print("Then let's move a point.")
            id = int(input("Which point ? "))
            print(sim.VUs[id])
            pos = torch.zeros(3)
            for j in range(3):
                pos[j] = float(input("What movement in " + ["x","y","z"][j] + " ?"))
            #sim.direct_movement(id,pos)
            k = int(input("How many frames of startup ?"))


        n = int(input("How many frames in total ?"))
        simStep = int(input("How many simulation steps per frames ?"))
        for i in tqdm(range(n + k)):
            force = torch.zeros(sim.omega.shape[0])
            for j in range(simStep):
                sim.add_step(force)
            if i < k:
                sim.direct_manip(id,pos,i/k * 100)
            tensorf.bender.param[0] = sim.get_displ()
            #print("This is my max disp",torch.max(torch.abs(tensorf.bender.param[0])))

            """
            for j in range(3):
                if j != om:
                    tensorf.bender.param[0,:,j] *= 0
            """

            if i < 0:
                print(tensorf.bender.param[0])

            W, H = test_dataset.img_wh

            rays_o, rays_d = get_rays(test_dataset.directions, c2w)  # both (h*w, 3)
            if ndc_ray:
                rays_o, rays_d = ndc_rays_blender(H, W, test_dataset.focal[0], 1.0, rays_o, rays_d)
            rays = torch.cat([rays_o, rays_d, torch.ones_like(rays_o)[:,0:1] * 0], 1)  # (h*w, 7)

            start = perf_counter()
            tensorf.bender.load_frames(torch.tensor([0]),0)
            stop = perf_counter()
            #print("Took",(stop - start),"seconds to fill the grid")
            start = perf_counter()
            rgb_map, _, depth_map, _, _ = renderer(rays, tensorf, chunk=4096, N_samples=N_samples,
                                        ndc_ray=ndc_ray, white_bg = white_bg, device=device, bending=True, deformation=deformation)
            stop = perf_counter()
            #print("Took", (stop - start), "seconds to render the thing")
            rgb_map = rgb_map.clamp(0.0, 1.0)

            rgb_map, depth_map = rgb_map.reshape(H, W, 3).cpu(), depth_map.reshape(H, W).cpu()

            #depth_map, _ = visualize_depth_numpy(depth_map.numpy(),near_far)

            rgb_map = (rgb_map.numpy() * 255).astype('uint8')
            if savePath is not None:
                imageio.imwrite(f'{savePath}/interaction{str(om)}/{prtx}{i:03d}.png', rgb_map)
                #rgb_map = np.concatenate((rgb_map, depth_map), axis=1)
                #imageio.imwrite(f'{savePath}/rgbd/{prtx}{i:03d}.png', rgb_map)

            if i == 0:
                rgb_map, _, depth_map, _, _ = renderer(rays, tensorf, chunk=4096, N_samples=N_samples,
                                                   ndc_ray=ndc_ray, white_bg=white_bg, device=device, bending=False,
                                                   deformation=deformation)
                rgb_map = rgb_map.clamp(0.0, 1.0)

                rgb_map = rgb_map.reshape(H, W, 3).cpu()
                #depth_map, _ = visualize_depth_numpy(depth_map.numpy(), near_far)
                if savePath is not None:
                    #rgb_map = np.concatenate((rgb_map, depth_map), axis=1)
                    imageio.imwrite(f'{savePath}/no_bending.png', rgb_map)

    return PSNRs

@torch.no_grad()
def interaction_modal_file(test_dataset,tensorf, n_fixed, renderer, savePath=None, N_vis=5, prtx='', N_samples=-1,
                    white_bg=False, ndc_ray=False, compute_extra_metrics=True, device='cuda',deformation=False,args=None):
    PSNRs, rgb_maps, depth_maps = [], [], []
    ssims,l_alex,l_vgg=[],[],[]
    os.makedirs(savePath, exist_ok=True)
    os.makedirs(savePath+"/rgbd", exist_ok=True)

    try:
        tqdm._instances.clear()
    except Exception:
        pass

    c2w = test_dataset.poses[int(n_fixed)]

    #tensorf.bender.generate_sinus()
    tensorf.bender.param.requires_grad = False

    # Let's init the simulation part
    canonical_sampling = tensorf.bender.canonical_sampling.detach()
    param = tensorf.bender.param.detach()  # t, k, 3
    param = torch.transpose(param, 1, 0)
    sim = modal.Simulation(torch.zeros((1)), torch.zeros((1, 3, 1)))

    sim.load(savePath)

    #sim.better_direct_movement(50000, torch.zeros(3))
    near_far = test_dataset.near_far

    if args.mode_script == "Nope":
       fileName = input("Please enter the name of the file you want to use as instruction.")
    else:
        fileName = args.mode_script
    try:
        f = open(fileName,"r")
    except:
        f = open("forces/p3.txt","r")

    line = f.readline()
    initial_guess = line.split(" ")
    line = f.readline()
    ending = -1
    instructions = []
    while line != "":
        newIns = line[:-1].split(" ")
        newIns[0] = int(newIns[0])
        if newIns[1] == "{camera":
            for i in range(int(newIns[-1])):
                instructions.append([newIns[0] + i,"camera",i + int(newIns[2])])
        elif newIns[1] == "{cameraBack":
            for i in range(int(newIns[-1])):
                instructions.append([newIns[0] + i,"camera",-i + int(newIns[2])])
        elif newIns[1] == "{pull":
            for i in range(int(newIns[-1])):
                instructions.append([newIns[0] + i,"pull",newIns[2],newIns[3],newIns[4],newIns[5],float(newIns[6])/int(newIns[-1]) * (i+1)])
        else:
            instructions.append(newIns)

        if ending == -1 and newIns[1] == "end":
            ending = newIns[0]
        line = f.readline()

    if ending == -1:
        ending = int(instructions[-1][0]) + 1
    os.makedirs(savePath + "/" + initial_guess[0], exist_ok=True)
    simStep = int(initial_guess[1])

    inst = [-1,""]
    tensorf.bender.accuracy_compute = False
    for i in tqdm(range(ending)):
        noInst = True
        while i == instructions[0][0]:
            noInst = False
            inst = instructions.pop(0)
            if inst[1] == "pull":
                direc = torch.zeros(3)
                for j in range(3):
                    direc[j] = float(inst[3 + j])
                sim.direct_manip(int(inst[2]), direc, float(inst[-1]))
            elif inst[1] == "force":
                force = torch.zeros(sim.omega.shape[0])
                for j in range(min(sim.omega.shape[0],len(inst[2:]))):
                    force[j] = float(inst[j + 2])
                sim.add_step(force)
            elif inst[1] == "camera":
                c2w = test_dataset.poses[int(inst[2])]
                force = torch.zeros(sim.omega.shape[0])
                for j in range(simStep):
                    sim.add_step(force)
            elif inst[1] == "eraseMode":
                listouille = []
                for j in range(2, len(inst)):
                    listouille.append(int(inst[j]))
                for j in listouille:
                    sim.VUs[:,:,j] *= 0.01
            elif inst[1] == "onlyShow":
                listouille = []
                for j in range(2,len(inst)):
                    listouille.append(int(inst[j]))
                for j in range(sim.VUs.shape[0]):
                    if not j in listouille:
                        sim.VUs[j] *= 0
            elif inst[1] == "rigidity":
                for j in range(min(sim.omega.shape[0], len(inst[2:]))):
                    sim.c[j] = float(inst[j + 2])

        if noInst:
            force = torch.zeros(sim.omega.shape[0])
            for j in range(simStep):
                sim.add_step(force)
        #print(inst)
        tensorf.bender.param[0] = sim.get_displ()
        if i == inst[0] and inst[1] == "vireToutLeMonde":
            tensorf.bender.param[0] = tensorf.bender.param[0] * 0 + 1000
            print("I did throw everyone out")
        #print("This is my max disp",torch.max(torch.abs(tensorf.bender.param[0])))

        W, H = test_dataset.img_wh

        rays_o, rays_d = get_rays(test_dataset.directions, c2w)  # both (h*w, 3)
        if ndc_ray:
            rays_o, rays_d = ndc_rays_blender(H, W, test_dataset.focal[0], 1.0, rays_o, rays_d)
        rays = torch.cat([rays_o, rays_d, torch.ones_like(rays_o)[:,0:1] * 0], 1)  # (h*w, 7)

        start = perf_counter()
        tensorf.bender.load_frames(torch.tensor([0]),0)
        stop = perf_counter()
        if i == -90:
            show_mythical_grid(tensorf,device)
        #print("Took",(stop - start),"seconds to fill the grid")
        start = perf_counter()
        rgb_map, _, depth_map, _, _ = renderer(rays, tensorf, chunk=4096, N_samples=N_samples,ndc_ray=ndc_ray, white_bg = white_bg, device=device, bending=True, deformation=deformation)
        stop = perf_counter()
        #print("Took", (stop - start), "seconds to render the thing")
        rgb_map = rgb_map.clamp(0.0, 1.0)

        rgb_map, depth_map = rgb_map.reshape(H, W, 3).cpu(), depth_map.reshape(H, W).cpu()

        #depth_map, _ = visualize_depth_numpy(depth_map.numpy(),near_far)

        rgb_map = (rgb_map.numpy() * 255).astype('uint8')
        if savePath is not None:
            imageio.imwrite(f'{savePath}/{initial_guess[0]}/{prtx}{i:03d}.png', rgb_map)
            #rgb_map = np.concatenate((rgb_map, depth_map), axis=1)
            #imageio.imwrite(f'{savePath}/rgbd/{prtx}{i:03d}.png', rgb_map)

        if i == 0:
            rgb_map, _, depth_map, _, _ = renderer(rays, tensorf, chunk=4096, N_samples=N_samples,
                                                   ndc_ray=ndc_ray, white_bg=white_bg, device=device, bending=False,
                                                   deformation=deformation)
            rgb_map = rgb_map.clamp(0.0, 1.0)

            rgb_map = rgb_map.reshape(H, W, 3).cpu()
            #depth_map, _ = visualize_depth_numpy(depth_map.numpy(), near_far)
            if savePath is not None:
                #rgb_map = np.concatenate((rgb_map, depth_map), axis=1)
                imageio.imwrite(f'{savePath}/no_bending.png', rgb_map)

    return PSNRs

def show_mythical_grid(tensorf,device):
    side = 200
    grid = np.mgrid[0:side, 0:side, 0:side].transpose((1, 2, 3, 0)).reshape((-1, 3))
    pts = torch.tensor(grid,device=device).view(-1,3) * 2 / side - 1

    side = 10
    grid = np.mgrid[0:side, 0:side, 0:side].transpose((1, 2, 3, 0)).reshape((-1, 3))

    shapyShape = (torch.tensor(grid,device=device).view(-1,3) * 2 / side - 1) * tensorf.bender.size_crop + tensorf.bender.new_c[None,:]

    canon = tensorf.bender.canonical_sampling * tensorf.bender.size_crop + tensorf.bender.new_c[None,:]
    sigma_canon = tensorf.compute_densityfeature(canon)
    sigma = tensorf.feature2density(sigma_canon)

    sMin = -1000
    idCanon = torch.tensor(range(canon.shape[0]))[sigma > sMin]
    canon = canon[sigma > sMin]
    ampl = torch.mean(torch.abs(tensorf.bender.param[0].detach()),dim=-1)
    ampl = ampl[sigma > sMin]
    displaced = canon + tensorf.bender.param[0][sigma > sMin]
    #print("Yo check out this shape dude ! UwU.",ampl.shape,canon.shape)


    sigma_feature = tensorf.compute_densityfeature(pts)
    sigma = tensorf.feature2density(sigma_feature) # Hopefully n*1

    real_pts = pts[sigma > 0.75]
    real_pts = real_pts[tensorf.bender.are_points_within_polytope(real_pts)]

    accuracy_pts = pts[sigma > 0.7]
    accuracy_pts = accuracy_pts[tensorf.bender.are_points_within_polytope(accuracy_pts)]
    tensorf.bender.accuracy_compute = True
    tensorf.bender.forward(accuracy_pts, torch.zeros(accuracy_pts.shape[0], 1, device=device))
    accuracy_scores = tensorf.bender.accuracy_score
    tensorf.bender.accuracy_compute = False


    app_features = tensorf.compute_appfeature(real_pts)
    valid_rgbs = tensorf.renderModule(real_pts, torch.ones_like(real_pts)*0.57735, app_features)
    #bended_rgbs = tensorf.renderModule(tensorf.bender.forward(real_pts,torch.zeros(real_pts.shape[0],1,device=device)), torch.ones_like(real_pts)*0.57735, app_features)

    ps.init()
    pts_cloud = ps.register_point_cloud("Sampled points",real_pts.cpu().detach().numpy())
    pts_cloud.add_color_quantity("Unbended colors",valid_rgbs.cpu().detach().numpy())
    pts_cloud2 = ps.register_point_cloud("Sampled points accuracy",accuracy_pts.cpu().detach().numpy())
    pts_cloud2.add_scalar_quantity("Scores",accuracy_scores.cpu().detach().numpy())
    ps.register_point_cloud("Deformation cage",shapyShape.cpu().detach().numpy())
    particles = ps.register_point_cloud("Canonical_particles",canon.cpu().detach().numpy())
    particles.add_scalar_quantity("amplitude",ampl.cpu().numpy())
    particles.add_scalar_quantity("id",idCanon.cpu().numpy())
    moved = ps.register_point_cloud("Moved_particles", displaced.cpu().detach().numpy())
    moved.add_scalar_quantity("amplitude",ampl.cpu().numpy())
    moved.add_scalar_quantity("id",idCanon.cpu().numpy())
    ps.show()
