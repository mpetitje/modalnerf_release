import numpy as np
import torch

# This is where it gets complicated

class Simulation:

    def __init__(self,omegas,VUs, step = 0.02, c = None, initial_guess = None):
        # omegas is a N-array,  VUs is a k,3,N-matrix   both are complex
        self.omega = omegas[:,None]
        self.q = torch.zeros((omegas.shape[0],1),dtype=complex)   # N,1
        self.step = step
        if c is not None:
            self.q = np.zeros((len(omegas),1)) # N
            self.c = c[:,np.newaxis]
            print("C :",c)
            self.VU = VUs # List of N MLPs
            self.s = np.ones_like(self.q)
            if initial_guess is not None:
                self.s *= initial_guess[:,np.newaxis] * 1 # NEED CORRECT THIS
            self.continuous = True
        else:
            print("SHapES",omegas.shape,VUs.shape)
            print(omegas)
            self.VU = np.concatenate([VUs[:, 0, :, np.newaxis], VUs[:, 1, :, np.newaxis], VUs[:, 2, :, np.newaxis]],axis=-1)  # k,N,3
            self.s = np.zeros_like(self.q)                     # N,3          Why is it this way ???
            self.c = 0.3 + 0 * np.real(self.omega) / 7
            self.divide = np.sum(np.real(self.VU), axis=-2) + np.sum(np.imag(self.VU), axis=-2)
            self.continuous = False

        self.history = np.array([[self.q,self.s,self.q - 1.j*self.s/self.omega]])
        self.real_omega = np.sqrt(self.omega*self.omega + self.c*self.c / 4)



    def add_step(self,f):

        #acc = f - (self.omega * self.omega + self.c * self.c / 4) * self.q - self.c * self.s
        #self.q = self.q + self.step * self.s
        #self.s = self.s + self.step * acc
        f = self.step * f + self.s
        self.s = f - self.q * (self.step * (self.omega * self.omega + self.c * self.c / 4))
        self.q = f * self.step + (1 + self.c * self.step) * self.q
        new_div = 1 / (1 + self.step * self.c + self.step * self.step * (self.omega * self.omega + self.c * self.c / 4))
        self.s = self.s * new_div
        self.q = self.q * new_div
        self.history = np.concatenate([self.history,np.array([[self.q,self.s,self.q - 1.j*self.s/self.real_omega]])],axis = 0)

    def get_disp(self, x=None):
        if self.continuous and x is not None:
            res = torch.zeros_like(x)
            for i in range(len(self.VU)):
                res += np.real(self.VU[i](x).detach().numpy() * (self.q[i] - 1.j*self.s[i]/self.real_omega[i]))
            return res
        return np.real(np.sum(self.VU * (self.q - 1.j*self.s/self.real_omega), axis = 1))

    def get_alien_disp(self,q):
        return np.real(np.sum(self.VU * q, axis=1))

    def get_q(self):
        return (self.q - 1.j*self.s/self.real_omega)

    def get_force(self,pos,dir, alpha = 40):
        dir = np.array(dir)
        f = dir * self.VU[pos[0]]
        f = f * alpha * self.omega
        return f

    def direct_manip(self,pos,dir,alpha = 1):
        dir = np.array(dir)
        f = dir[np.newaxis,:] * self.VU[pos[0]]
        magn = np.abs(f)
        angl = np.angle(f)
        f = np.exp(-1.0j*angl) * magn * alpha
        self.q = np.real(f)
        self.s = - self.real_omega * np.imag(f)

    def experimental_manip(self,pos, dir, alpha = 1):
        phi_divide = self.divide[pos[1],pos[0]] * self.divide[pos[1],pos[0]]
        phi = self.VU[pos[1],pos[0]]
        dir = np.array(dir) / phi_divide
        self.q = dir[np.newaxis,:] * np.real(phi)
        self.s = dir[np.newaxis, :] * np.imag(phi)


class TorchSimulation:
    def __init__(self,omegas, step, c, initial_speed):
        # omegas is a N-array,  VUs is a k,3,N-matrix   both are complex (not anymore !)
        self.omega = omegas[:]
        self.step = step
        self.c = c[:]
        self.continuous = True
        self.initial_speed = initial_speed
        #self.real_omega = np.sqrt(self.omega*self.omega + self.c*self.c / 4)

    def compute_steps(self,f):
        # f is like m,N with m the number of frames and N the number of modes
        steps = torch.zeros(f.shape[0],2,f.shape[1]).cuda()
        q = torch.zeros_like(self.initial_speed)
        s = self.initial_speed.clone()
        c = self.c * self.c
        for i in range(f.shape[0]):
            steps[i,0] = q
            steps[i,1] = s
            acc = f[i-1] - (self.omega * self.omega + c * c / 4) * q - c * s
            #print(acc.shape,f[i-1].shape,self.omega.shape,self.c.shape,steps[i-1,0].shape)
            q = q + self.step * s
            s = s + self.step * acc

        return steps

    """
    def get_disp(self, x=None):
        if self.continuous and x is not None:
            res = torch.zeros_like(x)
            for i in range(len(self.VU)):
                res += np.real(self.VU[i](x).detach().numpy() * (self.q[i] - 1.j*self.s[i]/self.real_omega[i]))
            return res
        return np.real(np.sum(self.VU * (self.q - 1.j*self.s/self.real_omega), axis = 1))
    """





