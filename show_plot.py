import os
os.environ['KMP_DUPLICATE_LIB_OK']='True'

import torch
import matplotlib.pyplot as plt
import numpy

name = "pendule3_7"
y = torch.load("log/" + name + "/"+name+"_grad.txt")
x = numpy.array([i for i in range(len(y))])

plt.plot(x[:],y[:])
plt.show()