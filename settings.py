import nvtx
import torch

do_measuring = False

def start_measure(message, color, domain, category):
    global do_measuring
    if do_measuring:
        torch.cuda.synchronize()
        return nvtx.start_range(message, color, domain, category)

def stop_measure(rng):
    global do_measuring
    if do_measuring:
        torch.cuda.synchronize()
        nvtx.end_range(rng)