scale = 0.0001
learning_rate = 0.1
normalize = True

# ----------------------------------

import torch 
import torch.nn.functional as F
import math

point = torch.tensor([0.0, 0.0], requires_grad=True) # point in [0, 1]
target = torch.tensor([1.0, 1.0], requires_grad=True)

opt = torch.optim.Adam([point], lr=learning_rate / scale if normalize else learning_rate)

for i in range(100):
    opt.zero_grad()
    world_space_point = scale * point
    loss = F.mse_loss(world_space_point, target)
    loss.backward()
    print(world_space_point)
    opt.step()



