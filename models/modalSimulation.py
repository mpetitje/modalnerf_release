import torch
from scipy.fft import fft, fftfreq

import numpy as np

class Simulation:
  def __init__(self,omega,VUs,step=0.002):
    # omega is N, VUs is k, 3, N, all complex
    self.load_omUv(omega,VUs)
    self.step = step

  def load_omUv(self,omega,VUs):
    self.omega = omega
    self.VUs = VUs
    self.q = torch.zeros((omega.shape[0]),dtype=torch.cfloat)
    self.s = torch.zeros((omega.shape[0]),dtype=torch.cfloat)
    self.c = torch.ones((omega.shape[0])) * 0.3

  def get_displ_old(self):
    return torch.sum(self.VUs * self.q[None,None,:],dim=-1).real

  def get_displ(self):
    return torch.sum(self.VUs * (self.q - 1.j*self.s/self.omega)[None,None,:],dim=-1).real

  def modal_analysis(self,pts):
    # pts have k, t, 2 shape
    pts = pts.numpy()
    timeline_x = fft(pts[:,:,0])
    timeline_y = fft(pts[:,:,1])
    timeline_z = fft(pts[:,:,2])
    print(timeline_y)

    realFreq = fftfreq(pts.shape[1],self.step)

    n_frames = pts.shape[1]
    plotable = np.mean(np.abs(np.real(timeline_x + timeline_y + timeline_z))[:,1:n_frames//2],axis=0)
    x = np.arange(1,n_frames // 2)

    timeline = np.concatenate([timeline_x[:,:,np.newaxis],timeline_y[:,:,np.newaxis],timeline_z[:,:,np.newaxis]],axis=-1)

    omegas = []
    UVs = []
    freq = 0
    plt.plot(x, plotable)
    plt.show()
    while freq >= 0 and freq < n_frames:

        freq = int(input("frequency ? "))
        if not (freq >= 0 and freq < n_frames):
            break
        if len(omegas) == 0:
            omegas.append(realFreq[freq])
            UVs = np.transpose(timeline[:, freq:freq+1, :],(0,2,1))
        elif not realFreq[freq] in omegas:
            omegas.append(realFreq[freq])
            UVs = np.concatenate([UVs, np.transpose(timeline[:, freq:freq+1, :],(0,2,1))], axis=2)
        else:
            print("Already taken, you fool !")
        print("ShApES",len(omegas),UVs.shape)

    self.load_omUv(torch.tensor(omegas,dtype = torch.cfloat),torch.tensor(UVs,dtype = torch.cfloat))
    print(omegas)


  def add_step(self,f):
    # f is of shape N
    f = self.step * f + self.s
    self.s = f - self.q * (self.step * (self.omega * self.omega + self.c * self.c / 4))
    self.q = f * self.step + (1 + self.c * self.step) * self.q
    new_div = 1 / (1 + self.step * self.c + self.step * self.step * (self.omega * self.omega + self.c * self.c / 4))
    self.s = self.s * new_div
    self.q = self.q * new_div

def main(args):
    ckpt = torch.load(args.ckpt, map_location=device)
    state = ckpt["bender"]
    canonical_sampling = state[0]
    param = state[1:].detach() # t, k, 3
    param = torch.transpose(param,1,0)

    sim = Simulation(torch.zeros((1)),torch.zeros((1,3,1)))

    sim.modal_analysis(param)


if __name__ == "__main__":
    import configargparse

    parser = configargparse.ArgumentParser()
    # mandatory arguments
    parser.add_argument(
        "--ckpt",
        type=str,
        help="The checkpoint to reload and perform the modal analysis on.",
    )

    args = parser.parse_args()

    main(args)


