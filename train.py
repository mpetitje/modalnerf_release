import os
os.environ['KMP_DUPLICATE_LIB_OK']='True'
#os.environ['CUDA_LAUNCH_BLOCKING'] = "1"

from tqdm.auto import tqdm
from opt import config_parser
import settings

from time import perf_counter

import json, random
from renderer import *
from utils import *
from rbf_render import *
from torch.utils.tensorboard import SummaryWriter
import datetime
import math
import os 
from debug_bender import DebugBender

from dataLoader import dataset_dict
import sys
import polytope as pt

from functools import partial
from torch.nn.parallel import DataParallel


device = torch.device("cuda" if torch.cuda.is_available() else "cpu")

_renderer = OctreeRender_trilinear_fast


class SimpleSampler:
    def __init__(self, total, batch):
        self.total = total
        self.batch = batch
        self.curr = total
        self.ids = None

    def nextids(self):
        self.curr+=self.batch
        if self.curr + self.batch > self.total:
            self.ids = torch.LongTensor(np.random.permutation(self.total))
            self.curr = 0
        return self.ids[self.curr:self.curr+self.batch]

class StraightSampler:
    def __init__(self,total,batch,cuts):
        cuts = cuts[0]
        self.total = total
        self.batch = batch
        self.curr = 0
        self.ids = torch.LongTensor(range(self.total))

        self.cuts = torch.cat([torch.zeros(1),cuts + 1, torch.tensor([self.total])])
        self.shuffle_batch()

    def shuffle_batch(self):
        # First we shuffle the rays of each frame
        shuffle = []
        for i in range(self.cuts.shape[0] - 1):
            shuffle.append(torch.randperm(int(self.cuts[i+1] - self.cuts[i])) + self.cuts[i])
        #self.ids = torch.cat(shuffle).to(torch.long)

        # Now we cut the rays into chunks and we shuffle the chunks around
        nChunk = math.ceil(self.total / self.batch)
        self.chunkPerm = torch.randperm(nChunk)
        #for i in range(nChunk):

    def nextids(self):
        self.curr += 1

        if (self.curr + 1) * self.batch > self.total:
            self.curr = 0
            self.shuffle_batch()

        i = self.chunkPerm[self.curr] * self.batch

        return self.ids[i:i+self.batch]

    def old_ids(self):
        self.curr += self.batch
        if self.curr + self.batch > self.total:
            self.curr = 0
            self.shuffle_batch()
        return self.ids[self.curr:self.curr + self.batch]

def test_sampler(sampler,rays):
    print("Quick sampler testing :")
    print(torch.where(rays == 74)[0].shape)
    print(torch.where(rays == 75)[0].shape)
    print(torch.where(rays == 0)[0].shape)
    print(torch.where(rays == 101)[0].shape)
    for i in range(5):
        e = sampler.nextids()
        v = rays[e]
        print("Batch number",i,": min",torch.min(v),"; max",torch.max(v))
        if torch.min(v) == 74:
            print("tiens tiens tiens un 74",e)
            print(torch.min(e),torch.max(e))

@torch.no_grad()
def export_mesh(args):

    ckpt = torch.load(args.ckpt, map_location=device)
    kwargs = ckpt['kwargs']
    kwargs.update({'device': device})
    tensorf = eval(args.model_name)(**kwargs)
    tensorf.load(ckpt, args)

    alpha,_ = tensorf.getDenseAlpha()
    convert_sdf_samples_to_ply(alpha.cpu(), f'{args.ckpt[:-3]}.ply',bbox=tensorf.aabb.cpu(), level=0.005)

def identite(x):
    return x

@torch.no_grad()
def render_test(args):
    # init dataset
    dataset = dataset_dict[args.dataset_name]
    test_dataset = dataset(args.datadir, split='test', downsample=args.downsample_train, is_stack=True, N_vis=-1, vis_begin=args.vis_begin, vis_end=args.vis_end, args=args)
    white_bg = test_dataset.white_bg
    ndc_ray = args.ndc_ray

    if not os.path.exists(args.ckpt):
        print('the ckpt path does not exists!!')
        return

    ckpt = torch.load(args.ckpt, map_location=device)
    ckpt["state_dict"].requires_grad = False
    if "bender" in ckpt:
        ckpt["bender"].requires_grad = False
    kwargs = ckpt['kwargs']
    kwargs.update({'device': device})
    kwargs['highlight_polytope'] = args.highlight_polytope
    tensorf = eval(args.model_name)(**kwargs)
    tensorf.load(ckpt, args)

    renderer = partial(_renderer, bending=not args.no_bending)

    if args.polytope == 1:
        pt.show_reduced_grid(tensorf,device)
        polytope = pt.load_polytope(args.datadir,test_dataset.poses[0],device)
        polytope = pt.resize_polytope(polytope,tensorf.aabb)
        polidx = pt.substract_polytope(tensorf.bender.canonical_sampling, polytope, device, True)
        if True:
            ps.set_use_prefs_file(False)
            pos,face = polytope
            ps.init()
            cano = ps.register_point_cloud("Canonical_sampling", tensorf.bender.canonical_sampling.detach().cpu().numpy())
            mark = torch.ones((tensorf.bender.canonical_sampling.shape[0]))
            mark[polidx] = 0
            cano.add_scalar_quantity("inside polytope", mark.numpy())
            ps.register_surface_mesh("Polytope", pos.detach().cpu().numpy(), face.detach().cpu().numpy(), smooth_shade=True)
            ps.show()
        tensorf.bender.substract(polidx)

    logfolder = os.path.dirname(args.ckpt)
    suffix = '_canonical' if args.no_bending else ''
    timestamp = datetime.datetime.now().isoformat(timespec="seconds").replace(":","")

    if args.render_train:
        os.makedirs(f'{logfolder}/imgs_train_all{suffix}/{timestamp}', exist_ok=True)
        train_dataset = dataset(args.datadir, split='train', downsample=args.downsample_train, is_stack=True, vis_begin=args.vis_begin, vis_end=args.vis_end)
        PSNRs_test = evaluation(train_dataset,tensorf, args, renderer, f'{logfolder}/imgs_train_all{suffix}/{timestamp}',
                                N_vis=-1, N_samples=-1, white_bg = white_bg, ndc_ray=ndc_ray,device=device)
        print(f'======> {args.expname} train all psnr: {np.mean(PSNRs_test)} <========================')

    if args.render_test:
        os.makedirs(f'{logfolder}/imgs_test_all/{timestamp}', exist_ok=True)
        evaluation(test_dataset,tensorf, args, renderer, f'{logfolder}/imgs_test_all/{timestamp}',
                                N_vis=-1, N_samples=-1, white_bg = white_bg, ndc_ray=ndc_ray,device=device,deformation=(args.render_def == 1))

    if args.render_fixed > -1:
        os.makedirs(f'{logfolder}/imgs_fixed_{args.render_fixed}/{timestamp}', exist_ok=True)
        evaluation_fixed(test_dataset, tensorf, args.render_fixed, renderer, f'{logfolder}/imgs_fixed_{args.render_fixed}/{timestamp}',
                        N_vis=-1, N_samples=-1, white_bg=white_bg, ndc_ray=ndc_ray, device=device, deformation=(args.render_def == 1))

    if args.render_path:
        c2ws = test_dataset.render_path
        os.makedirs(f'{logfolder}/imgs_path_all/{timestamp}', exist_ok=True)
        evaluation_path(test_dataset,tensorf, c2ws, renderer, f'{logfolder}/imgs_path_all/{timestamp}',
                                N_vis=-1, N_samples=-1, white_bg = white_bg, ndc_ray=ndc_ray,device=device)

    if args.render_modal >= 0:
        os.makedirs(f'{logfolder}/{args.expname}/imgs_fixed_{args.render_modal}', exist_ok=True)
        if args.load_modes:
            if args.load_modes == 1:
                interaction_modal(test_dataset, tensorf, args.render_modal, renderer,
                                 f'{logfolder}/{args.expname}/imgs_fixed_{args.render_modal}/',
                                 N_vis=-1, N_samples=-1, white_bg=white_bg, ndc_ray=ndc_ray, device=device,
                                 deformation=(args.render_def == 1))
            elif args.load_modes == 2:
                interaction_modal_file(test_dataset, tensorf, args.render_modal, renderer,
                                 f'{logfolder}/{args.expname}/imgs_fixed_{args.render_modal}/',
                                 N_vis=-1, N_samples=-1, white_bg=white_bg, ndc_ray=ndc_ray, device=device,
                                 deformation=(args.render_def == 1), args=args)
        else:
            evaluation_modal(test_dataset, tensorf, args.render_modal, renderer, f'{logfolder}/{args.expname}/imgs_fixed_{args.render_modal}/',
                        N_vis=-1, N_samples=-1, white_bg=white_bg, ndc_ray=ndc_ray, device=device, deformation=(args.render_def == 1))

def reconstruction(args):
    
    if args.add_timestamp:
        logfolder = f'{args.basedir}/{args.expname}{datetime.datetime.now().strftime("-%Y%m%d-%H%M%S")}'
    else:
        logfolder = f'{args.basedir}/{args.expname}'

    # init log file
    os.makedirs(logfolder, exist_ok=True)
    os.makedirs(f'{logfolder}/imgs_vis', exist_ok=True)
    os.makedirs(f'{logfolder}/imgs_rgba', exist_ok=True)
    os.makedirs(f'{logfolder}/rgba', exist_ok=True)
    summary_writer = SummaryWriter(logfolder)
    
    shutil.copyfile(args.config, f"{logfolder}/config.txt")

    # init dataset
    dataset = dataset_dict[args.dataset_name]
    train_dataset = dataset(args.datadir, split='train', downsample=args.downsample_train, is_stack=False, N_vis=-1, vis_begin=args.vis_begin, vis_end=args.vis_end, args=args)
    test_dataset = dataset(args.datadir, split='test', downsample=args.downsample_train, is_stack=True, N_vis=-1, vis_begin=args.vis_begin, vis_end=args.vis_end, args=args)
    white_bg = train_dataset.white_bg
    near_far = train_dataset.near_far
    ndc_ray = args.ndc_ray


    # init resolution
    upsamp_list = args.upsamp_list
    update_AlphaMask_list = args.update_AlphaMask_list
    n_lamb_sigma = args.n_lamb_sigma
    n_lamb_sh = args.n_lamb_sh

    # init parameters
    # tensorVM, renderer = init_parameters(args, train_dataset.scene_bbox.to(device), reso_list[0])
    aabb = train_dataset.scene_bbox.to(device)
    reso_cur = N_to_reso(args.N_voxel_init, aabb)
    nSamples = min(args.nSamples, cal_n_samples(reso_cur,args.step_ratio))

    bending_activated = not args.no_bending

    renderer = partial(_renderer, bending=bending_activated)


    if args.ckpt is not None:
        ckpt = torch.load(args.ckpt, map_location=device)
        kwargs = ckpt['kwargs']
        kwargs.update({'device':device})
        tensorf = eval(args.model_name)(**kwargs)
        tensorf.load(ckpt,args)
    else:
        tensorf = eval(args.model_name)(aabb, reso_cur, device,
                    density_n_comp=n_lamb_sigma, appearance_n_comp=n_lamb_sh, app_dim=args.data_dim_color, near_far=near_far,
                    shadingMode=args.shadingMode, alphaMask_thres=args.alpha_mask_thre, density_shift=args.density_shift, distance_scale=args.distance_scale,
                    pos_pe=args.pos_pe, view_pe=args.view_pe, fea_pe=args.fea_pe, featureC=args.featureC, step_ratio=args.step_ratio, fea2denseAct=args.fea2denseAct, fast_bending=args.fast_bending, remove_viewdir_in_polytope=args.remove_viewdir_in_polytope, highlight_polytope=args.highlight_polytope, rayMarch_weight_thres_polytope=args.rm_weight_mask_thre_polytope,
                    polytope_is_in_the_void=args.polytope_is_in_the_void)

        print("Hello ! Here is the number of images :",train_dataset.nImg)
        
        if bending_activated:
            if args.bender_type == "MLP":
                rayBender = MLP_Bender(32, train_dataset.nImg, device=device).cuda()
            else:
                if args.debug_bender:
                    rayBender = DebugBender(
                        num_frames=train_dataset.nImg, 
                        grid_size=2*args.grid_size,
                        origin=torch.tensor(args.polytope_pos).cuda(),
                        size=args.grid_size
                    )
                else:
                    pts_pos = torch.zeros((train_dataset.nImg, args.canonical_sampling_size, 3), requires_grad=True, device=device)
                    #pts_pos = torch.nn.Parameter(torch.zeros((train_dataset.nImg, args.canonical_sampling_size, 3),requires_grad = True, device=device))

                    rayBender = My_Multi_Opti_Bender(identite, torch.rand((args.canonical_sampling_size,3),device=device), pts_pos, grid_size = args.grid_size, k=args.cell_limit, poly_size=args.polytope_size, poly_pos=args.polytope_pos)
                    rayBender.generate_sampling()

                if not args.debug_bender:
                    print("Filling the grids.")
                    start = perf_counter()
                    rayBender.load_frames(torch.tensor(range(train_dataset.nImg)))
                    end = perf_counter()
                    print("Done filling the grids. Impressive.",(end - start),"seconds")

            tensorf.load_bender(rayBender)

    grad_vars = tensorf.get_optparam_groups(args.lr_init, args.lr_basis)
    if args.bender_type == "MLP":
        grad_vars = grad_vars[:-1]  # Let's remove this last parameter that is wrong and add the right ones
        grad_vars.append({'params':tensorf.bender.parameters(), 'lr':0.01})
        grad_vars.append({'params':tensorf.bender.ray_bending_latents_list,'lr':0.008})

    if args.lr_decay_iters > 0:
        lr_factor = args.lr_decay_target_ratio**(1/args.lr_decay_iters)
    else:
        args.lr_decay_iters = args.n_iters
        lr_factor = args.lr_decay_target_ratio**(1/args.n_iters)

    print("lr decay", args.lr_decay_target_ratio, args.lr_decay_iters)
    #print("Grad_vars",grad_vars)
    optimizer = torch.optim.Adam(grad_vars, betas=(0.9,0.99))


    #linear in logrithmic space
    N_voxel_list = (torch.round(torch.exp(torch.linspace(np.log(args.N_voxel_init), np.log(args.N_voxel_final), len(upsamp_list)+1))).long()).tolist()[1:]

    torch.cuda.empty_cache()
    PSNRs,PSNRs_test = [],[0]

    allrays, allrgbs = train_dataset.all_rays, train_dataset.all_rgbs

    #allrays[:,6] *= 0
    if not args.ndc_ray:
        allrays, allrgbs = tensorf.filtering_rays(allrays, allrgbs, bbox_only=True)
    #trainingSampler = SimpleSampler(allrays.shape[0], args.batch_size)
    trainingSampler = StraightSampler(allrays.shape[0], args.batch_size,torch.where(allrays[1:,6] - allrays[:-1,6] > 0))
    test_sampler(trainingSampler, allrays[:,6])

    Ortho_reg_weight = args.Ortho_weight
    print("initial Ortho_reg_weight", Ortho_reg_weight)

    L1_reg_weight = args.L1_weight_inital
    print("initial L1_reg_weight", L1_reg_weight)
    TV_weight_density, TV_weight_app = args.TV_weight_density, args.TV_weight_app
    tvreg = TVLoss()
    print(f"initial TV_weight density: {TV_weight_density} appearance: {TV_weight_app}")

    time_continuity_weight = args.time_continuity_weight
    print("Time continuity weight :",time_continuity_weight)

    deformation_reg_weight = args.deformation_reg_weight
    print("Deformation regularisation weight :",deformation_reg_weight)

    total_def_weight = args.total_def_weight
    print("Total deformation loss weight :", total_def_weight)
    if args.bender_type == "MLP":
        time_continuity_weight = 0
        deformation_reg_weight = 0
        total_def_weight = 0


    #print("All rays",torch.where((allrays[:-1,6] - allrays[1:,6]) != 0))
    grad_plot = []
    pbar = tqdm(range(args.n_iters), miniters=args.progress_refresh_rate, file=sys.stdout)

    
    if "PARALLEL" in os.environ:
        parallel_tensorf = torch.nn.DataParallel(tensorf).cuda()
    else:
        parallel_tensorf = tensorf

    for iteration in pbar:

        iteration_rng = settings.start_measure("iteration", "red", "training", "iteration")
        if iteration % 100 == -10:
            print("Sampling first values", tensorf.bender.get_displ()[0,0:2], grad_vars[-1]["params"][0][0,0:2], grad_vars[0]["params"][0][0,0,0])

        ray_idx = trainingSampler.nextids()
        rays_train, rgb_train = allrays[ray_idx], allrgbs[ray_idx].to(device)

        load_rng = settings.start_measure("loading", "red", "training", "iteration")
        if bending_activated and not args.debug_bender: # On a 30000 iterations run this should only add 2 minutes and 30 seconds
            m = int(torch.min(rays_train[:,6]))
            M = int(torch.max(rays_train[:,6]))
            tensorf.bender.load_frames(torch.tensor(range(m,M+1)), m)
            #tensorf.bender.load_frames(torch.tensor(range(train_dataset.nImg)), 0)
        settings.stop_measure(load_rng)

        #rgb_map, alphas_map, depth_map, weights, z_vals
        rgb_map, alphas_map, depth_map, weights, z_vals = renderer(rays_train, parallel_tensorf, chunk=args.batch_size, N_samples=nSamples, white_bg = white_bg, ndc_ray=ndc_ray, device=device, is_train=True)
        #rgb_map2, _, _, _, _ = renderer(rays_train, tensorf, chunk=args.batch_size, N_samples=nSamples, white_bg=white_bg, ndc_ray=ndc_ray, device=device, is_train=True, bending=False)

        #print("Is it the same rgb map ?",torch.mean((rgb_map - rgb_map2)**2)," ???")

        loss = torch.mean((rgb_map - rgb_train) ** 2) #+ 0 * torch.abs(torch.mean(tensorf.bender.get_displ() - 1))
        #loss = torch.abs(torch.mean(tensorf.bender.get_displ() - 1))

        #pts = torch.rand((8,3,3),device=device)
        #bend = torch.zeros((8,1),device=device)[:,None,:].expand((8,3,1))
        #bend = bend.reshape((-1,1))
        #new_pts = torch.reshape(tensorf.bender(torch.reshape(pts,(-1,3)),bend),(8,3,3))
        #pts = torch.stack((pts,pts))
        #loss = torch.mean(pts - new_pts)

        # loss
        total_loss = loss
        if Ortho_reg_weight > 0:
            loss_reg = tensorf.vector_comp_diffs()
            total_loss += Ortho_reg_weight*loss_reg
            summary_writer.add_scalar('train/reg', loss_reg.detach().item(), global_step=iteration)
        if L1_reg_weight > 0:
            loss_reg_L1 = tensorf.density_L1()
            total_loss += L1_reg_weight*loss_reg_L1
            summary_writer.add_scalar('train/reg_l1', loss_reg_L1.detach().item(), global_step=iteration)

        if TV_weight_density>0:
            TV_weight_density *= lr_factor
            loss_tv = tensorf.TV_loss_density(tvreg) * TV_weight_density
            total_loss = total_loss + loss_tv
            summary_writer.add_scalar('train/reg_tv_density', loss_tv.detach().item(), global_step=iteration)

        if TV_weight_app>0:
            TV_weight_app *= lr_factor
            loss_tv = tensorf.TV_loss_app(tvreg)*TV_weight_app
            total_loss = total_loss + loss_tv
            summary_writer.add_scalar('train/reg_tv_app', loss_tv.detach().item(), global_step=iteration)

        if args.distortion_loss_weight > 0.0:
            from torch_efficient_distloss import eff_distloss, eff_distloss_native, flatten_eff_distloss
            distortion_loss = eff_distloss(weights, z_vals, 1/z_vals.shape[1])
            total_loss = total_loss + distortion_loss * args.distortion_loss_weight
            summary_writer.add_scalar('train/reg_distortion', distortion_loss.detach().item(), global_step=iteration)

        if args.time_continuity_weight > 0 and bending_activated and not args.debug_bender:
            time_loss = torch.mean(torch.abs(tensorf.bender.param[1:] - tensorf.bender.param[:-1]))

            total_loss = total_loss + time_loss * args.time_continuity_weight
            summary_writer.add_scalar('train/reg_time', time_loss.detach().item(), global_step=iteration)

        if args.deformation_reg_weight > 0 and bending_activated and not args.debug_bender:
            deformation_loss = tensorf.bender.spatial_reg()
            total_loss = total_loss + deformation_loss * args.deformation_reg_weight
            summary_writer.add_scalar('train/reg_deform', deformation_loss.detach().item(), global_step=iteration)

        if total_def_weight > 0 and bending_activated and not args.debug_bender:
            total_def_loss = torch.mean(torch.abs(tensorf.bender.param))
            total_loss = total_loss + total_def_loss * args.total_def_weight
            summary_writer.add_scalar('train/reg_total_def', total_def_loss.detach().item(), global_step=iteration)

        backward_rng = settings.start_measure("backward", "yellow", "training", "iteration")
        optimizer.zero_grad()
        total_loss.backward()
        settings.stop_measure(backward_rng)
        #print("Grad after (there is supposed to be something here)",torch.max(grad_vars[-1]["params"][0].grad) - torch.min(grad_vars[-1]["params"][0].grad))

        # if iteration % 20 >= 0 and bending_activated and not args.debug_bender:
        #     grad_plot.append(float(torch.max(torch.abs(grad_vars[-1]["params"][0].grad))))
            #print(grad_plot[-1])
            #print(torch.mean(torch.abs(tensorf.bender.param),dim=1)[:5])

        optimization_rng = settings.start_measure("optimization", "green", "training", "iteration")
        optimizer.step()
        settings.stop_measure(optimization_rng)

        loss = loss.detach().item()
        
        PSNRs.append(-10.0 * np.log(loss) / np.log(10.0))
        summary_writer.add_scalar('train/PSNR', PSNRs[-1], global_step=iteration)
        summary_writer.add_scalar('train/mse', loss, global_step=iteration)

        for param_group in optimizer.param_groups:
            param_group['lr'] = param_group['lr'] * lr_factor

        # Print the current values of the losses.
        if iteration % args.progress_refresh_rate == 0:
            #print("Sampling mean",torch.mean(tensorf.bender.get_displ()))
            pbar.set_description(
                f'Iteration {iteration:05d}:'
                # + f' train_psnr = {float(np.mean(PSNRs)):.2f}'
                # + f' test_psnr = {float(np.mean(PSNRs_test)):.2f}'
                # + f' mse = {loss:.6f}'
            )
            PSNRs = []

        if iteration % args.vis_every == args.vis_every - 1 and args.N_vis!=0:
            PSNRs_test = evaluation(test_dataset,tensorf, args, renderer, f'{logfolder}/imgs_vis/', N_vis=args.N_vis,
                                    prtx=f'{iteration:06d}_', N_samples=nSamples, white_bg = white_bg, ndc_ray=ndc_ray, compute_extra_metrics=False)
            summary_writer.add_scalar('test/psnr', np.mean(PSNRs_test), global_step=iteration)
            tensorf.save(f'{logfolder}/{args.expname}_{str(iteration)}.th')

        if iteration in update_AlphaMask_list:
            if reso_cur[0] * reso_cur[1] * reso_cur[2]<256**3:# update volume resolution
                reso_mask = reso_cur
            new_aabb = tensorf.updateAlphaMask(tuple(reso_mask))
            if iteration == update_AlphaMask_list[0]:
                tensorf.shrink(new_aabb)
                # tensorVM.alphaMask = None
                L1_reg_weight = args.L1_weight_rest
                print("continuing L1_reg_weight", L1_reg_weight)

            if not args.ndc_ray and iteration == update_AlphaMask_list[1]:
                # filter rays outside the bbox
                allrays,allrgbs = tensorf.filtering_rays(allrays,allrgbs)
                #trainingSampler = SimpleSampler(allrgbs.shape[0], args.batch_size)
                trainingSampler = StraightSampler(allrays.shape[0], args.batch_size, torch.where(allrays[1:, 6] - allrays[:-1, 6] > 0))

        if iteration in upsamp_list:
            n_voxels = N_voxel_list.pop(0)
            reso_cur = N_to_reso(n_voxels, tensorf.aabb)
            nSamples = min(args.nSamples, cal_n_samples(reso_cur,args.step_ratio))
            tensorf.upsample_volume_grid(reso_cur)

            if args.lr_upsample_reset:
                print("reset lr to initial")
                lr_scale = 1 #0.1 ** (iteration / args.n_iters)
            else:
                lr_scale = args.lr_decay_target_ratio ** (iteration / args.n_iters)
            grad_vars = tensorf.get_optparam_groups(args.lr_init*lr_scale, args.lr_basis*lr_scale, args.lr_particles*(lr_scale if args.decay_lr_particles else 1.0))
            if args.bender_type == "MLP":
                grad_vars = grad_vars[:-1]  # Let's remove this last parameter that is wrong and add the right ones
                grad_vars.append({'params': tensorf.bender.parameters(), 'lr': 0.01})
                grad_vars.append({'params': tensorf.bender.ray_bending_latents_list, 'lr': 0.008})

            optimizer = torch.optim.Adam(grad_vars, betas=(0.9, 0.99))

        settings.stop_measure(iteration_rng)

    tensorf.save(f'{logfolder}/{args.expname}.th')

    # torch.save(grad_plot,f'{logfolder}/{args.expname}_grad.txt')

    timestamp = datetime.datetime.now().isoformat(timespec="seconds")
    
    if args.render_train:
        suffix = '_canonical' if args.no_bending else ''
        os.makedirs(f'{logfolder}/imgs_train_all{suffix}/{timestamp}', exist_ok=True)
        train_dataset = dataset(args.datadir, split='train', downsample=args.downsample_train, is_stack=True, vis_begin=args.vis_begin, vis_end=args.vis_end)
        PSNRs_test = evaluation(train_dataset,tensorf, args, renderer, f'{logfolder}/imgs_train_all{suffix}/{timestamp}',
                                N_vis=-1, N_samples=-1, white_bg = white_bg, ndc_ray=ndc_ray,device=device)
        print(f'======> {args.expname} test all psnr: {np.mean(PSNRs_test)} <========================')

    if args.render_test:
        os.makedirs(f'{logfolder}/imgs_test_all/{timestamp}', exist_ok=True)
        PSNRs_test = evaluation(test_dataset,tensorf, args, renderer, f'{logfolder}/imgs_test_all/{timestamp}',
                                N_vis=-1, N_samples=-1, white_bg = white_bg, ndc_ray=ndc_ray,device=device)
        summary_writer.add_scalar('test/psnr_all', np.mean(PSNRs_test), global_step=iteration,compute_extra_metrics=False)
        print(f'======> {args.expname} test all psnr: {np.mean(PSNRs_test)} <========================')

    if args.render_fixed > -1:
        os.makedirs(f'{logfolder}/imgs_fixed_{args.render_fixed}/{timestamp}', exist_ok=True)
        evaluation_fixed(test_dataset, tensorf, args.render_fixed, renderer, f'{logfolder}/imgs_fixed_{args.render_fixed}/{timestamp}',
                        N_vis=-1, N_samples=-1, white_bg=white_bg, ndc_ray=ndc_ray, device=device)

    if args.render_path:
        c2ws = test_dataset.render_path
        # c2ws = test_dataset.poses
        print('========>',c2ws.shape)
        os.makedirs(f'{logfolder}/imgs_path_all/{timestamp}', exist_ok=True)
        evaluation_path(test_dataset,tensorf, c2ws, renderer, f'{logfolder}/imgs_path_all/{timestamp}',
                                N_vis=-1, N_samples=-1, white_bg = white_bg, ndc_ray=ndc_ray,device=device)

    return f'{logfolder}/{args.expname}.th'

if __name__ == '__main__':
    torch.set_default_dtype(torch.float32)
    torch.manual_seed(20211202)
    np.random.seed(20211202)

    settings.do_measuring = True

    args = config_parser()
    print(args)

    if args.export_mesh:
        export_mesh(args)

    if args.render_only:
        print("We are indeed in render only mode, thank you very much.")
        render_test(args)
    else:
        args.ckpt = reconstruction(args)
        if "RENDER_AFTER_TRAINING" in os.environ:
            args.cell_limit = 12
            args.step_ratio = 0.5
            args.render_fixed = 1
            args.render_train = 1
            render_test(args)
            if not args.no_bending:
                args.no_bending = True
                render_test(args)


