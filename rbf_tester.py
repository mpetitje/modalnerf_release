import torch
from time import perf_counter
device = torch.device("cuda" if torch.cuda.is_available() else "cpu")
from rbf_render import *
import polyscope as ps
from tqdm.auto import tqdm
import polytope as pt

ps.set_use_prefs_file(False)
ps.init()

canonical_sampling = torch.rand((100000,3),device=device) * 0
img_displ = torch.ones((250,100000,3),device=device) * 0.12
img_displ[:,:,0] *= 0
img_displ[:,:,1] *= 0

def identite(x):
    return x

benderA = My_Multi_Opti_Bender(identite,canonical_sampling,img_displ)
benderA.generate_sampling()
canonical_sampling = benderA.canonical_sampling
img_displ[:,:,2] *= torch.sin(canonical_sampling[:,1] * 10)[None,:]

benderA.load_frames(torch.tensor([0,1]))
cloud_name = torch.zeros((canonical_sampling.shape[0]),device=device)
pts = torch.tensor([-0.5,-0.1,-0.2],device=device) + torch.tensor([1,0.5,0.5],device=device) * torch.linspace(0,1,50,device=device)[:,None]
pts = torch.cat([pts + i * 0.08 for i in range(10)],dim=0)
idx = torch.zeros((500),device=device).to(torch.long)
idx[:10] = 1

neigh = benderA.get_neighbours(pts,idx)
print(neigh.shape)
tagColor = torch.zeros((canonical_sampling.shape[0]))

#ps.init()

origin = pts.clone()
ps.register_point_cloud("input points",pts.clone().cpu().detach().numpy())

for i in range(5):
    tagColor[neigh[i]] = i+1
    #print(torch.max(tagColor),tagColor.shape)
    ps.register_point_cloud("Neighbours " + str(i), (canonical_sampling + img_displ[0])[neigh[i]].cpu().detach().numpy())

result_pts = benderA(pts,idx[:,None])

ps_result = ps.register_point_cloud("Result_pts",result_pts.detach().cpu().numpy())
ps_cloud = ps.register_point_cloud("Deformation field", (canonical_sampling + img_displ[0]).cpu().detach().numpy())
tup = benderA.get_tuple((canonical_sampling + img_displ[0]), idx)
realTup = tup[:,0] + benderA.grid_size * tup[:,1] + benderA.grid_size * benderA.grid_size * tup[:,2]
ps_cloud.add_scalar_quantity("grid position",realTup.cpu().detach().numpy())
ps_cloud.add_scalar_quantity("neighbours",(tagColor).cpu().detach().numpy())

for i in range(5):
    print(origin[40 + i],result_pts[40 + i])

ps.show()

print("Testing neighbour detection")



idx = torch.ones((1,1),device=device).to(torch.long)
#pts = torch.tensor([0.0154,0.0422,-0.0557],device=device)
#print(benderA.get_tuple(pts,idx))
#realNeigh = torch.where(torch.sum((benderA.param[1] - pts)**2,dim=-1) < benderA.cell_size * 0.25 * benderA.cell_size)
#print(realNeigh[0].shape[0])
#tt = benderA.get_tuple(benderA.param[1][realNeigh[0]],torch.ones((realNeigh[0].shape[0],1),device=device).to(torch.long))
#print(tt)

for i in tqdm(range(50000)):
    pts = torch.rand(1,3,device=device) * 2 - 1
    neigh = benderA.get_neighbours(pts,idx)
    realNeigh = torch.where(torch.sum((canonical_sampling + benderA.param[1] - pts)**2,dim=-1) < benderA.cell_size * 0.25 * benderA.cell_size)
    for j in range(realNeigh[0].shape[0]):
        if not realNeigh[0][j] in neigh:
            print("found a mistake")#,neigh,realNeigh,pts,realNeigh[0][j])
            continue
            print(benderA.get_tuple(pts,idx),pts,realNeigh[0].shape[0])
            ghostId = realNeigh[0][j]
            print(ghostId, (canonical_sampling + benderA.param[1])[ghostId])
            tt = benderA.get_tuple((canonical_sampling + benderA.param[1])[ghostId], idx)
            print(tt)
            print(benderA.grid[1,tt[0],tt[1],tt[2]])
            neighs = {}
            for k in range(realNeigh[0].shape[0]):
                tt = benderA.get_tuple((canonical_sampling + benderA.param[1])[realNeigh[0][k]],idx)
                tt = str(list(tt.detach().cpu()))
                if tt in neighs:
                    neighs[tt] += 1
                else:
                    neighs[tt] = 1
            print(neighs)

print("Done")

"""
for i in range(25,250,25):
    start = perf_counter()
    for j in range(100):
        benderA.load_frames(torch.tensor(range(i)))

    end = perf_counter()
    print("Filling",i,"grids at a time takes :",(end - start)/100,"s  This is",(end-start)/100 * 30000 / 3600,"hours of training.")
"""

input_pts = torch.rand((2000000,3),device=device)
frames = torch.floor(torch.rand((2000000,1),device=device) * 100)
benderA.load_frames(torch.tensor(range(101)))

start = perf_counter()
for j in range(100):
    benderA(input_pts,frames)

end = perf_counter()
print("Querying the grid with two million pts :",(end - start)/100,"s  This is",(end-start)/100 * 30000 / 3600,"hours of training.")

