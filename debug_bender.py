import torch 
import torch.nn as nn 
import torch.nn.functional as F

class DebugBender(torch.nn.Module):
    def __init__(self, num_frames: int, grid_size: int, origin: torch.Tensor, size: float = 1.0):
        super().__init__()

        self.grid_size = grid_size
        self.num_frames = num_frames 
        self.origin = origin
        self.size = size
        
        _displacements = torch.zeros(self.num_frames, 3, self.grid_size, self.grid_size, self.grid_size)
        self.displacements = torch.nn.Parameter(_displacements.cuda())

    def load_frames(self, *args, **kwargs):
        pass

    def are_points_within_polytope(self, points):
        return torch.max(torch.abs(points - self.origin), dim=-1)[0] <= self.size

    def forward(self, points, frame_idx):
        local_points = (points - self.origin) / self.size

        displaced_local_points = torch.zeros_like(points)
        unique_idx, idx_mapping = frame_idx.long().unique(return_inverse=True)

        displacements = torch.zeros_like(local_points)
        for i in unique_idx:
            mask = idx_mapping.squeeze(-1) == i
            samples = F.grid_sample(self.displacements[i:i+1], local_points[mask][None, None, None], mode="bilinear", align_corners=False)
            displacements[mask] = samples.squeeze(0).squeeze(1).squeeze(1).t()
        
        displaced_points = local_points + displacements
        return (displaced_points * self.size) + self.origin


class CacheBender(torch.nn.Module):
    def __init__(self, bender):
        super().__init__()
        self.bender = bender

    def forward(self, points, img_idx):
        k = self.bender.canonical_side
        
        xyz = self.bender.canonical_sampling.detach()
        idx = img_idx if (img_idx.ndim == 0 or img_idx.shape[0] == 0) else torch.ones(self.bender.canonical_sampling.shape[0], 1).cuda() * img_idx[0,0]
        displacement_grid = self.bender(xyz, idx, normalize_inputs=False).reshape(k, k, k, 3).permute(3, 0, 1, 2)[None]
        
        return points + F.grid_sample(displacement_grid, points[None, None, None], mode="bilinear", align_corners=False).squeeze(0).squeeze(1).squeeze(1).T
