import torch
import modalSimulation as modal
import numpy as np
import matplotlib.pyplot as plt
from tqdm import trange

from scipy.fft import fft, fftfreq


def main(args):

    data = torch.load(args.ckpt)["bender"]
    canonical = data[0]
    target = data[1:].detach() # Let's only look at the first 20 points

    # Artificially building some sort of periodic function
    #target[:,0,0] = torch.sin(torch.arange(0,5,step=5/target.shape[0])) * 0.004
    #target[:,0,1:] *= 0

    #omegaParam = torch.rand(args.nModes,requires_grad=True)
    omegaParam = torch.arange(0.05,3.05,step = 3/args.nModes,requires_grad=True)
    cParam = torch.ones(args.nModes, requires_grad=True)
    VUsParamReal = torch.ones((target.shape[1],3,args.nModes),requires_grad=True)
    VUsParamComplex = torch.ones((target.shape[1],3,args.nModes),requires_grad=True)
    forceParam = torch.ones((target.shape[0],args.nModes),requires_grad=True)

    realFreq = fftfreq(target.shape[0], 0.02)
    maxFreq = abs(realFreq[target.shape[0]//2])
    print("Max freq is",maxFreq)
    maxFreq = maxFreq * 0.8
    # But for real tho we probably wanna ignore the higher frequencies so we should maybe reduce the maxFreq by 25% ? 50% ? We'll see.
    omegas = (torch.exp(-torch.abs(omegaParam))) * maxFreq # Here put the real range of possible frequencies
    VUs = VUsParamReal + 1.j * VUsParamComplex

    sim = modal.Simulation(omegas,VUs,step=0.02)
    #sim.modal_analysis(torch.transpose(target.cpu(), 1, 0))

    param = [{'params':omegaParam, 'lr':0.0005},{'params':VUsParamReal, 'lr':0.0005},{'params':VUsParamComplex, 'lr':0.0005},{'params':forceParam, 'lr':0.0001},{'params':cParam, 'lr':0.0001}]
    optimizer = torch.optim.Adam(params=param, betas=(0.9, 0.999))
    y = []
    for i in trange(1,3000):

        omegas = (torch.exp(-torch.abs(omegaParam))) * maxFreq  # Here put the real range of possible frequencies
        VUs = VUsParamReal + 1.j * VUsParamComplex
        sim.load_omUv(omegas,VUs)
        sim.c = torch.exp(-torch.abs(cParam))

        result = torch.zeros_like(target) # frames * k * 3
        #sim.add_step(forceParam * 1)
        for j in range(result.shape[0]):
            sim.add_step(forceParam[j] * 0.002)
            result[j] = sim.get_displ()


        #loss = torch.mean(torch.linalg.norm(result - target, dim=-1))
        loss = torch.mean(torch.abs(result - target))
        #dist_loss = torch.mean(torch.abs(torch.abs(sim.omega[1:] - sim.omega[:-1]) - maxFreq / args.nModes)) * 10
        #loss = loss + dist_loss
        loss.backward(retain_graph=False)
        y.append(float(loss.cpu()))
        optimizer.step()

        if i == 1500 or i == 1000:
            for p in param:
                p['lr'] *= 0.5


    print("It's over now !")
    sim.save(args.save)
    print("Omega",sim.omega)
    print("Omega param",omegaParam)
    print("c",cParam)
    #print(forceParam)
    plt.plot([i for i in range(len(y))], y)
    #plt.yscale("log")
    plt.show()
    print("Here is the movement")
    target = target.detach().cpu().numpy()
    result = result.detach().cpu().numpy()
    x = list(range(target.shape[0]))
    fig, ax = plt.subplots()
    ax.plot(x, [target[i,0,0] for i in range(target.shape[0])], label='ground_truth x')
    ax.plot(x, [result[i,0,0] for i in range(target.shape[0])], label='modal_training x')
    #ax.plot(a, c + d, 'k', label='Total message length')

    legend = ax.legend(loc='upper right', shadow=True, fontsize='x-large')
    plt.show()
    fig, ax = plt.subplots()
    ax.plot(x, [target[i, 0, 1] for i in range(target.shape[0])], label='ground_truth y')
    ax.plot(x, [result[i, 0, 1] for i in range(target.shape[0])], label='modal_training y')
    # ax.plot(a, c + d, 'k', label='Total message length')

    legend = ax.legend(loc='upper right', shadow=True, fontsize='x-large')
    plt.show()



if __name__ == "__main__":
    import configargparse

    parser = configargparse.ArgumentParser()
    # mandatory arguments
    parser.add_argument(
        "--ckpt",
        type=str,
        help="The checkpoint to reload and perform the modal analysis on.",
    )
    parser.add_argument(
        "--save",
        type=str,
        help="The place to save the thing.",
    )
    parser.add_argument("--nModes",type=int,help="Number of modes to train.",default=8)

    args = parser.parse_args()

    main(args)